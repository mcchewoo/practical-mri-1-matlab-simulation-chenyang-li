function  displaysequencediagram_2adc(time,rfPulse,Gx,Gy,Gz,adcx,adcy)

subplot(6,1,1)
plot(time, rfPulse)
title('RF waveform')
xlabel('time')
ylabel('B1/T')

subplot(6,1,2)
plot(time, Gx)
title('X Gradient')
xlabel('time')
ylabel('T/m')

subplot(6,1,3)
plot(time, Gy)
title('Y Gradient')
xlabel('time')
ylabel('T/m')

subplot(6,1,4)
plot(time, Gz)
title('Z Gradient')
xlabel('time')
ylabel('T/m')

subplot(6,1,5)
plot(time, adcx)
title('ADCx')
xlabel('time/s')

subplot(6,1,6)
plot(time, adcy)
title('ADCy')
xlabel('time/s')

end

