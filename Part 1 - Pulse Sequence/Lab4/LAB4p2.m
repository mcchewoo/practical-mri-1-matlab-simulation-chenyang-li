%% Exercise 4.3
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same

dt    = 10^-5; 
gamma = 42.577*10^6;

%load the voxel model
load('PD.mat');
load('T1.mat');
load('T2.mat');

%Allocate the memory needed
nTimeSteps  = 200;
rfPulse     = zeros(1,nTimeSteps); %variable to hold a RF waveform
gradAmp     = zeros(3,nTimeSteps); %variable to hold a gradient waveform
time        = zeros(1,nTimeSteps); %variable to hold the time points

xSteps  = size(T1,1);
ySteps  = size(T1,2);
zSteps  = 1;

dX = 4.0e-3;
dY = 4.0e-3;
dZ = 1.0e-4;

% 3D positions in space

pos = zeros(3,xSteps,ySteps,zSteps);
% Explanation on pos: This four dimensions matrix indicates three dimension
% indexes and once we identified the location by index, we know the xyz
% position information in the 3 row matrix.
for k=1:xSteps
    for j=1:ySteps
        for i=1:zSteps
            pos(1,k,j,i) = (k-xSteps/2)*dX;
            pos(2,k,j,i) = (j-ySteps/2)*dY;
            pos(3,k,j,i) = (i-zSteps/2)*dZ;
        end
    end
end


% Generates the time line for plotting
for i=1:nTimeSteps 
    time(i)    = i*10^-5;                       %Time in seconds
end

% Basic Calculation for rf Pulse:
% T = 0.001s; TBWP = 3 ---> df = 3000 ---> t0 = 1/3000 ----> 1/t0 = 3000;
% Z Gradient calculation:
% According to Eq: dz = 2*pi*df/(gamma*Gz)

dz = 0.005;
df = 3000;
Gz = df/(gamma*dz); % Calculating Gz 

% Generate the excitation waveform (90 degree pulse)
area = 0;
FA = pi/2;
hanning_window = hann(100)';
for i=1:100 
    rfPulse(i)   =  hanning_window(i)*sinc(3000*(i-50)*10^-5);  
    area = area + rfPulse(i)*dt;
    gradAmp(3,i) =  Gz;        % Z gradients in Tesla per meter
end

sinc_90_amp = FA/(2*pi*gamma*area);
rfPulse = rfPulse*sinc_90_amp;

vali_FA = 0;
for i = 1:100
 vali_FA = vali_FA + gamma*2*pi*rfPulse(i)*dt;
end
% This is to test if the result is right, it is correct if vali_FA is pi/2(1.57)

% Generate the readout gradients:
% From the Eq. of Readout Gradient Design: Gx = 4*pi*BW/(2*pi*gamma*Lx);
% Tacq = nx*dt ---> BW = nx/(2*Tacq);

Lx = 0.192;                            % Lx is FOV in x direction;
nx = 48;                               % nx is number of data point
Tacq = nx*dt;                          % Tacq is the acquisition time
BW_receiver = nx/(2*Tacq);             % Receiver Bandwidth
Gx = 4*pi*BW_receiver/(2*pi*gamma*Lx); % Gx = 0.0122 T/m

for i=151:(151+size(T1,1))
     gradAmp(1,i) = Gx;        % X gradients in Tesla per meter
end

%Generate the pre-phasor and slice refocusing gradients

% phase encoding gradient calculation:
Ly = 0.192;                            % Ly is FOV in y direction
N = 48;                                % N is number of phase encoding step
Area_pe_max = pi*(N)/(2*pi*gamma*Ly);   % Area under the largest phase encoding
Gymax = Area_pe_max/(50*dt);       % gradAmp(2,i) = 0.0057 T/m (largest gradient)

for i=101:150
    gradAmp(1,i) =  -24*Gx/50;   % X gradients in Tesla per meter(prephasing)
    gradAmp(2,i) = Gymax;      % Y gradients(max) in Tesla per meter
    gradAmp(3,i) =  -Gz;       % Z gradients in Tesla per meter(refocusing)        
end

% Question B:

adc = zeros(1,length(rfPulse)); % variable to hold an ADC 

for i = 1:nTimeSteps
    if i > 150 && i <= (150+size(T1,1)) 
        adc(i) = i-150;
    end
end


%% Question A:
displaysequencediagram(time,rfPulse,gradAmp(1,:),gradAmp(2,:),gradAmp(3,:),adc);
print('lab4p2_sequence_diagram','-dpng');
%% Question B:

kSpace  = zeros(size(T1,1),size(T1,2));

tic
for trIndex=1:size(T1,2)

disp(trIndex);    

% update the phase endcoding gradient:

   for t=101:148
      %gradAmp(2,t) =  pi*(2*(trIndex-24)-1)/(2*pi*gamma*Ly*50*dt); %Y gradients in Tesla per meter
      % gradAmp(2,t) = -Gymax + (trIndex-1)*2*Gymax/47;
      gradAmp(2,t) = Gymax - 2*(trIndex-1)*Gymax/47;
   end

for k=1:xSteps  
    for j=1:ySteps
        for i=1:zSteps
            
            dB0 = pos(:, k, j, i)'*gradAmp(:,1);  % dot product between G(t).r 
            [mT,mZ] =  bloch(dt, dB0,rfPulse(1), T1(k,j),T2(k,j), 0, 1);   % start from fully relaxed spin state
    
            for t=2:nTimeSteps %t starts at 2

                dB0 = pos(:, k, j, i)'*gradAmp(:,t); 
                [mT,mZ] =  bloch(dt, dB0,rfPulse(t), T1(k,j),T2(k,j), mT, mZ); 
                
                if(adc(t)>0)
                    %Sum the signal over all spins and store in k-space
                    %Don't forget to wigh the signal with the PD.
                    kSpace(round(adc(t)),trIndex) = kSpace(round(adc(t)),trIndex) + mT * PD(k,j);
                end
                
            end  %end of time loop          
            
            
        end %end of i loop
    end %end of j loop
end %end of k loop

end %end of TR loop
toc

%% Question E:
clf;

imshow(abs(log(kSpace)),[]); % Show logarithmic scale of k-space data

print('Lab4p2_QuestionC_kspace_log','-dpng');
clf;

imshow(abs(kSpace),[]);

print('Lab4p2_QuestionC_kspace','-dpng');

clf;

i = ifftshift(ifft2(fftshift(kSpace)));
imshow(abs(i));
title('The reconstructed image from k space')
print('Lab4p2_QuestionC_FTimage','-dpng');

% Answer: It is proton density weighted imaging.
