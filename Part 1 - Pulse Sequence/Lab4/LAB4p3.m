%% Exercise 4.3
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same

dt    = 10^-5; 
gamma = 42.577*10^6;

%load the voxel model
load('PD.mat');
load('T1.mat');
load('T2.mat');

%Allocate the memory needed
nTimeSteps  = 200*48;
rfPulse     = zeros(1,nTimeSteps); %variable to hold a RF waveform
gradAmp     = zeros(3,nTimeSteps); %variable to hold a gradient waveform
adc         = zeros(2,nTimeSteps); %variable to hold a gradient waveform
time        = zeros(1,nTimeSteps); %variable to hold the time points


xSteps  = size(T1,1);   %Number of simulated "spins" in the x directions 
ySteps  = size(T1,2);   %Number of simulated "spins" in the y directions 
zSteps  = 1;            %Number of simulated "spins" in the z directions 

dX = 4.0e-3;            %Distance between simulated "spins" in the x directions  [meter]
dY = 4.0e-3;            %Distance between simulated "spins" in the y directions  [meter]
dZ = 1.0e-4;            %Distance between simulated "spins" in the z directions  [meter]


% 3D positions in space
pos = zeros(3,xSteps,ySteps,zSteps);
for k=1:xSteps
    for j=1:ySteps
        for i=1:zSteps
            pos(1,k,j,i) = (k-xSteps/2)*dX;
            pos(2,k,j,i) = (j-ySteps/2)*dY;
            pos(3,k,j,i) = (i-zSteps/2)*dZ;
        end
    end
end

%Generates the time line for sequence plotting
for i=1:nTimeSteps 
    time(i)    = i*dt;                       %Time in seconds
end

%Question A

% Generate the first excitation and calculation for excitation pulse:

dz = 0.005;
df = 3000;
Gz = df/(gamma*dz); % Calculating Gz 

% Generate the excitation waveform (15 degree pulse)
area = 0;
FA = pi/6;
hanning_window = hann(100)';

for i=1:100 
    rfPulse(i)   =  hanning_window(i)*sinc(3000*(i-50)*10^-5);  
    area = area + rfPulse(i)*dt;
    gradAmp(3,i) =  Gz;                     %Z gradients in Tesla per meter
end

sinc_30_amp = FA/(2*pi*gamma*area);
rfPulse = rfPulse*sinc_30_amp;

vali_FA = 0;
for i = 1:100
 vali_FA = vali_FA + gamma*2*pi*rfPulse(i)*dt;
end

Lx = 0.192;                            % Lx is FOV in x direction;
nx = 48;                               % nx is number of data point
Tacq = nx*dt;                          % Tacq is the acquisition time
BW_receiver = nx/(2*Tacq);             % Receiver Bandwidth
Gx = 4*pi*BW_receiver/(2*pi*gamma*Lx); % Gx = 0.0122 T/m

% Generate the readout gradients and add the ADC event
for i=151:(150+size(T1,1))
     gradAmp(1,i) = Gx;                %X gradients in Tesla per meter
     adc(1,i) = i-150;
     adc(2,i) = 1;
end
% phase encoding gradient calculation:
Ly = 0.192;                            % Ly is FOV in y direction
N = 48;                                % N is number of phase encoding step
Area_pe_max = pi*48/(2*pi*gamma*Ly);   % Area under the largest phase encoding
Gymax = Area_pe_max/(50*dt);       % gradAmp(2,i) = 0.0057 T/m (largest gradient)

%Generate the pre-phasor, phase encoding and  slice refocusing gradients
for i=101:150
    gradAmp(1,i) =  -24*Gx/50; %X gradients in Tesla per meter
    gradAmp(2,i) =  Gymax;   %Y gradients in Tesla per meter
    gradAmp(3,i) =  -Gz;      %Z gradients in Tesla per meter         
end


%% Question A:

for i=1:47                          % 47 TR to iterate the pulse
    nStart =(200*i)+1;              % Specify index of start/end point for different TR
    nEnd   =(200*i)+200;
    rfPulse(:,nStart:nEnd) = rfPulse(:,1:200); % flip the RF pulse back and forth
    
    gradAmp(1,nStart:nEnd) = gradAmp(1,1:200); 
    %
    gradAmp(2,nStart+100:nStart+149) = gradAmp(2,101:150) - 2*i*Gymax/47;
    
    gradAmp(3,nStart:nEnd) = gradAmp(3,1:200);
   
    adc(1,nStart:nEnd)     = adc(1,1:200);
    adc(2,nStart:nEnd)     = adc(2,1:200)*(i+1);
end

%plot the compleate sequence

displaysequencediagram_2adc(time,rfPulse,gradAmp(1,:),gradAmp(2,:),gradAmp(3,:),adc(1,:),adc(2,:));
print('lab4p3_sequence_diagram','-dpng');
%% Question B:

kSpace  = zeros(size(T1,1),size(T1,2));

tic
    
for k=1:xSteps
    disp(k);
    for j=1:ySteps
        for i=1:zSteps
             
            dB0 = pos(:, k, j, i)'*gradAmp(:,1);
            [mT,mZ] =  bloch(dt, dB0,rfPulse(1), T1(k,j),T2(k,j), 0, 1);
    
            for t= 2:nTimeSteps %i starts at 2

                dB0 = pos(:, k, j, i)'*gradAmp(:,t);
                [mT,mZ] =  bloch(dt, dB0,rfPulse(t), T1(k,j),T2(k,j), mT, mZ);
                
                if(adc(1,t)>0)
                    kSpace(round(adc(2,t)),round(adc(1,t))) = kSpace(round(adc(2,t)),round(adc(1,t))) + mT * PD(k,j);
                end
                
            end            
        end
    end
end

toc

%% Question C:

clf;

imshow(abs(log(kSpace)),[]); % Show logarithmic scale of k-space data
title('K-space on logarithmic scale');
print('Lab4p3_QuestionC_kspace_log','-dpng');

clf;
imshow(abs(kSpace),[]);
title('K-space Raw Data');
print('Lab4p3_QuestionC_kspace','-dpng');

clf;
i = ifftshift(ifft2(fftshift(kSpace)));
imshow(abs(i),[]);
title('FT reconstructed GRE image - No spoiling');

print('Lab4p3_QuestionC_FTimage','-dpng');


% The reconstructed image looks very bad. It is because that artefacts come 
% from the signals generated by the previous exciatation, because after each short TR, 
% the transverse magnetization has very limited (not enought) time to get fully recovered back to 0.
% Therefore, taransverse magnetization will be mixed up in the next TR, making the signal 
% messy and not correct.