function displaysequence(time,rfPulse,gradAmp)

subplot(3,1,1)
plot(time, angle(rfPulse),'LineWidth',2)
title('RF Phase')
xlabel('time')
ylabel('Phase')

subplot(3,1,2)
plot(time, abs(rfPulse),'LineWidth',2)
title('RF Amplitude')
xlabel('time')
ylabel('Amplitude')

subplot(3,1,3)
plot(time, gradAmp,'LineWidth',2)
title('Gradient Amplitude')
xlabel('time')
ylabel('Amplitude')

end

