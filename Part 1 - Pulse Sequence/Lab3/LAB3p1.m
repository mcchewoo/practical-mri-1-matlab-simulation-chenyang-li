%% Exercise 3.1
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same


dt    = 10^-7; 
gamma = 42.577*10^6;


%Allocate the memory needed
nTimeSteps  = 30000;
rfPulse     = zeros(1,nTimeSteps); %variable to hold a RF waveform with 10000 samples
gradAmp     = zeros(1,nTimeSteps); %variable to hold a gradient waveform with 10000 samples
time        = zeros(1,nTimeSteps); %variable to hold 10000 time points

nPosSteps  = 200;
posZ       = zeros(1,nPosSteps); %variable to hold nPosSteps positions allong the z direction
mFinalVect = zeros(nPosSteps,2); %variable to hold the final magnetization calculated for each position


%Generates the time line for plotting
for i=1:nTimeSteps %i starts at 1 go's to 15000
    time(i)    = i*10^-7;                       %Time in seconds
end

%Generate a list of sampling points allong the z direction
for i=1:nPosSteps %i starts at 1 go's to 200
    posZ(i)  = (i-100)*10^-4; %Distance from iso center in meters
end  

%Generate the RF Inversion waveform
for i=1:20000 %i starts at 1 go's to 10000
    rfPulse(i) = (sin(pi*i/20000)^2)*sinc(pi*(i-10000)/19000)*10^-5; %B1+ in Tesla  
    gradAmp(i) = 10*10^-3;                       %Tesla per meter
end

for i=20001:30000 %i starts at 1 go's to 10000
     gradAmp(i) = -gradAmp(1000);  
end

% gradAmp = -gradAmp;
% Why gradient is negative flips the profile?
%Question 0:

displaysequence(time,rfPulse,gradAmp);
print('Lab3p1_question0_RFwaveform','-dpng');

%Question B:
tic;
for j=1:nPosSteps
    mT = 0;
    mZ = 1;

    gradAtPosJ = posZ(j)*gradAmp(1);
    [mT,mZ] =  bloch(dt,gradAtPosJ,rfPulse(1),2,2,mT,mZ,1);
    
    for i=2:nTimeSteps %i starts at 2
        gradAtPosJ = posZ(j)*gradAmp(i);
        [mT,mZ] =  bloch(dt,gradAtPosJ,rfPulse(i),2,2,mT,mZ,1);
    end

    mFinalVect(j,:) = [mT, mZ];
end
toc;

clf;
subplot(3,1,1);
plot(posZ, abs(mFinalVect(:,1)),'LineWidth',2);
title('xy magnitude');
ylabel('magnitude');
	
subplot(3,1,2);
plot(posZ, angle(mFinalVect(:,1)),'LineWidth',2);
title('xy phase');
ylabel('Phase');

subplot(3,1,3);
plot(posZ, mFinalVect(:,2),'LineWidth',2);
title('z magnitude');
ylabel('magnitude');
xlabel('z Position');

print('Lab3p1_QuestionB','-dpng');

%Question C

clf;
load('Lab3p1_ref.mat');
subplot(2,1,1);
plot(posZ, abs(mFinalVect(:,1)),'b',posZ,abs(refM(:,1)),'r--');
title('xy magnitude comparison');
ylabel('magnitude');
legend('simulation','reference');
	
subplot(2,1,2);
plot(posZ, angle(mFinalVect(:,1)),'b',posZ,angle(refM(:,1)),'r--');
title('xy phase');
ylabel('Phase');
legend('simulation','reference');

print('Lab3p1_QuestionC_compare','-dpng');

%QuestionD:
% Elapsed time is 3.494238 seconds.It is approximately 2 times slower than
% fastsim