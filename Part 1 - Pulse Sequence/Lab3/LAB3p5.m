%% Exercise 3.4
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same


dt    = 10^-5; 
gamma = 42.577*10^6;

te = [2 3 4 6 8 12 16 24 32 48 64 96 128 192 256 310 640 800]*10^-3; % te in ms

%Allocate the memory needed
nTimeSteps  = 10000;
rfPulseE    = zeros(1,200); %exciation
rfPulse     = zeros(1,nTimeSteps); %variable to hold a RF waveform with 10000 samples
gradAmp     = zeros(1,nTimeSteps); %variable to hold a gradient waveform with 10000 samples
time        = zeros(1,nTimeSteps); %variable to hold 10000 time points

tiSteps    = size(te,2);
nSpins     = 1000;
spins      = zeros(1,nSpins); %variable to hold nPosSteps positions allong the z direction
mFinalVect = zeros(tiSteps,2); %variable to hold the final magnetization calculated for each position
tmp        = zeros(nSpins,2);

%Generates the time line for plotting
for i=1:nTimeSteps %i starts at 1 go's to 15000
    time(i)    = i*dt;                       %Time in seconds
end

%Question A:

% Generate random dB0:

dB0_min = -2.5*10^-7;
dB0_max = 2.5*10^-7;

% Spin #: 1000:
dB0_T2star = dB0_min + (dB0_max-dB0_min)*rand(1000,1);

dB0_T2star = mean(dB0_T2star);

%T2* decay simulation:

area = 0; % initiating area
FA = 0.5*pi;

hanning_window = hann(200)';
for i = 1:200
    rfPulse(i) = hanning_window(i)*sinc(1500*(i-100)*10^-5); % Pulse function
    % Calculating the area under the RF Pulse
    area = area + rfPulse(i)*dt;   
end

%rfPulse = pi*rfPulse/area;
sinc_90_amp = FA/(2*pi*gamma*area);
 
rfPulse = rfPulse*sinc_90_amp;

valid_FA = 0;
for i = 1:200
 valid_FA = valid_FA + gamma*2*pi*rfPulse(i)*dt; % This is to validate result
end

%Question B:

%Generate pulse sequence diagram by different TE:
for j = 1:length(te)
    mT = 0;
    mZ = 1;
    
    for i = 1:200
    rfPulse(i) = hanning_window(i)*sinc_90_amp*sinc(1500*(i-100)*10^-5);
    end
    
    for i = 1:(10000*te(j)+100)
        [mT,mZ] =  bloch(dt,0,rfPulse(i),0.85,0.06,mT,mZ,1);
        [mT,mZ] =  bloch(dt,dB0_T2star,0,0.85,0.06,mT,mZ,1);
    end
    
    mFinalVect(j,:) = [mT, mZ];
end

d = imag(mFinalVect(:,1));

%Question C:

load('T2_decay_signal_Lab3p4.mat');

plot(te,c,'r*',te,d,'b*');
legend('T2','T2*');
xlabel('Time/s');
ylabel('Mxy/T')
title('Comparison between T2 and T2*');

%Question D:

[xData, yData] = prepareCurveData( te, d );

% Set up fittype and options.
ft = fittype( 'exp(-b*x)', 'independent', 'x', 'dependent', 'y' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = 0.299046063647193;

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );

% Plot fit with data.
figure( 'Name', 'exp(-t/T2*)' );
h = plot( fitresult, xData, yData );
legend( h, 'd vs. te', 'exp(-t/T2*)', 'Location', 'NorthEast' );
% Label axes
xlabel te/s
ylabel Mxy(T2*)/T
title('T2* decay')
grid on

% As the fitting curve indicates, T2* is approximately 2 times faster than
% T2 if fitting in the same equation.

