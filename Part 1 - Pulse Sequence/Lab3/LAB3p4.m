%% Exercise 3.4
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same


dt    = 10^-5; 
gamma = 42.577*10^6;

te = [2 3 4 6 8 12 16 24 32 48 64 96 128 192 256 310 640 800]*10^-3; % te in ms

%Allocate the memory needed
nTimeSteps  = 10000;
rfPulseE    = zeros(1,200); %exciation
rfPulse     = zeros(1,nTimeSteps); %variable to hold a RF waveform with 10000 samples
gradAmp     = zeros(1,nTimeSteps); %variable to hold a gradient waveform with 10000 samples
time        = zeros(1,nTimeSteps); %variable to hold 10000 time points

tiSteps  = size(te,2);
% posZ       = zeros(1,tiSteps); %variable to hold nPosSteps positions allong the z direction
mFinalVect = zeros(tiSteps,2); %variable to hold the final magnetization calculated for each position


%Generates the time line for plotting
for i=1:nTimeSteps %i starts at 1 go's to 15000
    time(i)    = i*dt;                       %Time in seconds
end


% Generate 90 degree sinc pulse:
% T = 0.002s; TBWP = 3; BW = 1500; 1/t0 = 1500; because we calculate the
% 180 pulse amplitude before, therefore the 90 pulse is half the amplitude.

area = 0; % initiating area
FA = 0.5*pi;


for i = 1:200
    rfPulse(i) = sinc(1500*(i-100)*10^-5); % Pulse function
    % Calculating the area under the RF Pulse
    area = area + rfPulse(i)*dt;   
end

%rfPulse = pi*rfPulse/area;
sinc_90_amp = FA/(2*pi*gamma*area);
 
rfPulse = rfPulse*sinc_90_amp;

valid_FA = 0;
for i = 1:200
 valid_FA = valid_FA + gamma*2*pi*rfPulse(i)*dt; % This is to validate result
end

% Question A:

%Generate pulse sequence diagram by different TE:
for j = 1:length(te)
    
    mT = 0;
    mZ = 1;
    
    for i = 1:200
    rfPulse(i) = sinc_90_amp*sinc(1500*(i-100)*10^-5);
    end
    
    dB0AtPosJ = 0;
    [mT,mZ] =  bloch(dt,dB0AtPosJ,rfPulse(1),0.85,0.06,mT,mZ,1);
    
    for i = 2:(10000*te(j)+100)
        [mT,mZ] =  bloch(dt,dB0AtPosJ,rfPulse(i),0.85,0.06,mT,mZ,1);
    end
    
    mFinalVect(j,:) = [mT, mZ];
end

c = abs(mFinalVect(:,1));
d = angle(mFinalVect(:,1));

%Question B: plot amplitude and phase against TE:
clf;
subplot(2,1,1);
plot(te,c,'+','LineWidth',2);
xlabel('Echo Time/s');
ylabel('signal amplitude');
title('signal amplitude as functiion of TE');

subplot(2,1,2);
plot(te,d,'b--','LineWidth',2);
xlabel('Echo Time/s');
ylabel('signal phase');
title('signal phase as functiion of TE');

print('Lab3p4_questionB','-dpng');

save('T2_decay_signal_Lab3p4.mat','c');

%% Question C:

% curve fitting:
% Fit: 'exp(-t/T2)'.
[xData, yData] = prepareCurveData( te, c );

% Set up fittype and options.
ft = fittype( 'exp(-b*x)', 'independent', 'x', 'dependent', 'y' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = 0.918748101947052;

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );

% Plot fit with data.
figure( 'Name', 'exp(-t/T2)' );
h = plot( fitresult, xData, yData );
legend( h, 'c vs. te', 'exp(-t/T2)', 'Location', 'NorthEast' );
% Label axes
xlabel te
ylabel c
grid on

print('lab3p4_questionC_fitting','-dpng');

% As the curve fitting tool indicates, the estimated T2 is 0.6s

%% Question D:

TE = [0.012 0.024 0.048];

area = 0; % initiating area
FA = 0.5*pi;


for i = 1:200
    rfPulse(i) = sinc(1500*(i-100)*10^-5); % Pulse function
    % Calculating the area under the RF Pulse
    area = area + rfPulse(i)*dt;   
end

%rfPulse = pi*rfPulse/area;
sinc_90_amp = FA/(2*pi*gamma*area);
 
rfPulse = rfPulse*sinc_90_amp;

valid_FA = 0;
for i = 1:200
 valid_FA = valid_FA + gamma*2*pi*rfPulse(i)*dt; % This is to validate result
end


%Generate pulse sequence diagram by different TE (only 3 TE):
for j = 1:length(TE)
    
    mT = 0;
    mZ = 1;
    
    for i = 1:200
    rfPulse(i) = sinc_90_amp*sinc(1500*(i-100)*10^-5);
    end
    
    dB0AtPosJ = 0;
    [mT,mZ] =  bloch(dt,dB0AtPosJ,rfPulse(1),0.85,0.06,mT,mZ,1);
    
    for i = 2:(10000*TE(j)+100)
        [mT,mZ] =  bloch(dt,dB0AtPosJ,rfPulse(i),0.85,0.06,mT,mZ,1);
    end
    
    mFinalVect2(j,:) = [mT, mZ];
end

e = abs(mFinalVect2(:,1));
f = angle(mFinalVect2(:,1));

clf;
subplot(2,1,1);
plot(TE,e,'+','LineWidth',2);
xlabel('Echo Time/s');
ylabel('signal amplitude');
title('signal amplitude as functiion of TE');

subplot(2,1,2);
plot(TE,f,'b--','LineWidth',2);
xlabel('Echo Time/s');
ylabel('signal phase');
title('signal phase as functiion of TE');

print('Lab3p4_questionC_3TE','-dpng');

% after fitting by cftool type in the command line, the result does not
% affected, T2 is still 0.6s.