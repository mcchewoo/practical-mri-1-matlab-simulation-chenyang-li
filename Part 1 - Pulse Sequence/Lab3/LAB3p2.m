%% Exercise 3.1
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same


dt    = 10^-5; 
gamma = 42.577*10^6;


%Allocate the memory needed
nTimeSteps  = 300;
rfPulse     = zeros(1,nTimeSteps); %variable to hold a RF waveform with 10000 samples
gradAmp     = zeros(1,nTimeSteps); %variable to hold a gradient waveform with 10000 samples
time        = zeros(1,nTimeSteps); %variable to hold 10000 time points

nPosSteps  = 200;
posZ       = zeros(1,nPosSteps); %variable to hold nPosSteps positions allong the z direction
mFinalVect = zeros(nPosSteps,2); %variable to hold the final magnetization calculated for each position


%Generates the time line for plotting
for i=1:nTimeSteps %i starts at 1 go's to 15000
    time(i)    = i*10^-5;                       %Time in seconds
end

%Generate a list of sampling points allong the z direction
for i=1:nPosSteps %i starts at 1 go's to 200
    posZ(i)  = (i-100)*10^-4; %Distance from iso center in meters
end  

%Generate the RF Inversion waveform
for i=1:200 %i starts at 1 go's to 10000
    rfPulse(i) = (sin(pi*i/200)^2)*sinc(pi*(i-100)/190)*10^-5; %B1+ in Tesla  
    gradAmp(i) = 10*10^-3;                       %Tesla per meter
end

for i=201:300 %i starts at 1 go's to 10000
     gradAmp(i) = -gradAmp(10);  
end

%Question 0:

displaysequence(time,rfPulse,gradAmp);
print('Lab3p2_question0_RFwaveform','-dpng');

%Question B:
tic;
for j=1:nPosSteps
    mT = 0;
    mZ = 1;

    gradAtPosJ = posZ(j)*gradAmp(1);
    [mT,mZ] =  bloch(dt,gradAtPosJ,rfPulse(1),2,2,mT,mZ,1);
    
    for i=2:nTimeSteps %i starts at 2
        gradAtPosJ = posZ(j)*gradAmp(i);
        [mT,mZ] =  bloch(dt,gradAtPosJ,rfPulse(i),2,2,mT,mZ,1);
    end

    mFinalVect(j,:) = [mT, mZ];
end
toc;

subplot(3,1,1);
plot(posZ, abs(mFinalVect(:,1)),'LineWidth',2);
title('xy magnitude');
ylabel('magnitude');
	
subplot(3,1,2);
plot(posZ, angle(mFinalVect(:,1)),'LineWidth',2);
title('xy phase');
ylabel('Phase');

subplot(3,1,3);
plot(posZ, mFinalVect(:,2),'LineWidth',2);
title('z magnitude');
ylabel('magnitude');
xlabel('z Position');

print('Lab3p2_QuestionB','-dpng');

% The slice profile is accurate. 

%Question C

%Elapsed time is 0.0519725 seconds.
