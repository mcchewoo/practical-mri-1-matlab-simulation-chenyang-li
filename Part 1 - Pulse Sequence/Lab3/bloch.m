function [ mtOut, mzOut  ] = bloch(dt,dB0,B1,T1,T2,mt,mz,mz0) %1sec

   gammaBar = 2*pi*42.577*10^6; %Hz per Tesla
    
   %calulate mtOut:
   
   mtOut = 1i*sin(gammaBar*dt*B1)*mz;
   
   mtOut = mtOut + cos(gammaBar*dt*imag(B1))*real(mt) + 1i*cos(gammaBar*dt*real(B1))*imag(mt);
  
   %and calculate mzOut:
   
   mzOut = cos(gammaBar*B1*dt)*mz;
   
   mzOut = mzOut + sin(gammaBar*dt*imag(B1))*real(mt) - sin(gammaBar*dt*real(B1))*imag(mt);
   
   %apply B0:
   
   mtOut = mtOut*exp(1i*gammaBar*dt*dB0);
   
   %apply relaxation:
   
   %T2 relaxation on transverse magnetization:
   mtOut = exp(-dt/T2)*mtOut;
   %T1 relaxation on longitudinal magnetization:
   mzOut = mzOut * exp(-dt/T1) + mz0*(1-exp(-dt/T1));
   

end