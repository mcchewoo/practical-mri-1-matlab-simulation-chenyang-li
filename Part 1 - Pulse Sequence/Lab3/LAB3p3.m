%% Exercise 3.3
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same

dt    = 10^-5; 
gamma = 42.577*10^6;

ti = [25 50 100 200 400 800 1600 3200 6400]*10^-3; % ti in s

%Allocate the memory needed
nTimeSteps  = 700000;
rfPulseI    = zeros(1,400); %inversion
rfPulseE    = zeros(1,200); %excitation
rfPulse     = zeros(1,nTimeSteps);%variable to hold a RF waveform with 10000 samples
gradAmp     = zeros(1,nTimeSteps); %variable to hold a gradient waveform with 10000 samples
time        = zeros(1,nTimeSteps); %variable to hold 10000 time points

tiSteps  = size(ti,2); % 9 rows
% posZ       = zeros(1,tiSteps); %variable to hold nPosSteps positions allong the z direction
mFinalVect = zeros(tiSteps,2); %variable to hold the final magnetization calculated for each position


%Generates the time line for plotting
for i=1:nTimeSteps %i starts at 1 go's to 15000
    time(i)    = i*dt;                       %Time in seconds
end

%Question A:

% inversion 180 degree pulse calculation and design:
% T = 0.004s, TBWP = 5, therefore the BW = 1250Hz,t0 = 0.004/5, 1/t0=1250

area = 0;
FA = pi;
for i = 1:400
    rfPulseI(i) = sinc(1250*(i-200)*10^-5);
    area = area + rfPulseI(i)*dt; 
end
sinc_180_amp = FA/(2*pi*gamma*area);
rfPulseI = rfPulseI*sinc_180_amp;

Valid_FA_180 = 0;
for i = 1:400
 Valid_FA_180 = Valid_FA_180 + gamma*2*pi*rfPulseI(i)*dt;
end

area2 = 0;
FA2 = 0.25*pi;

% 45 degree excitation pulse calculation and design:
% T = 0.002s, TBWP = 3, therefore BW = 1500Hz, t0 = 1/1500s,1/t0 = 1500;
for i = 401:600
    rfPulseE(i) = sinc(1500*(i-500)*10^-5);
    area2 = area2 + rfPulseE(i)*dt; 
end

sinc_45_amp = FA2/(2*pi*gamma*area2);
rfPulseE = rfPulseE*sinc_45_amp;

Valid_FA_45 = 0;
for i = 401:600
 Valid_FA_45 = Valid_FA_45 + gamma*2*pi*rfPulseE(i)*dt;
end

% The inversion pulse is generated, next, we need to know the index of 45 degree 
% pulse because it is changing with respect to the TI.

for j = 1:length(ti)
    
    for i =1:nTimeSteps   % Reset RF Pulse to zero after each TI loop
        rfPulse(i) = 0;
    end
    
    mT = 0;   
    mZ = 1;
    
    %hanning_window = hann(400)';
    for i = 1:400
        rfPulse(i) = sinc_180_amp*sinc(1250*(i-200)*10^-5);
    end    
    
    for i = (10^5*ti(j)+100):(10^5*ti(j)+300)
        rfPulse(i) = sinc_45_amp*sinc(1500*(i-(10^5*ti(j)+200))*10^-5);
    end
    
    dB0AtPosJ = 0;
    
    [mT,mZ] =  bloch(dt,dB0AtPosJ,rfPulse(1),0.85,0.06,0,1,1);
    
    for i = 2:(10^5*ti(j)+500)
      
    [mT,mZ] =  bloch(dt,dB0AtPosJ,rfPulse(i),0.85,0.06,mT,mZ,1);
    
    end 
    
    mFinalVect(j,:) = [mT, mZ];
    
end

%Question B:
a = imag(mFinalVect(:,1));
b = angle(mFinalVect(:,1));

% plot amplitude and phase against TI:
clf;
subplot(2,1,1);
plot(ti,a,'+','LineWidth',2);
xlabel('Inversion Time/s');
ylabel('signal amplitude');
title('signal amplitude as functiion of TI');

subplot(2,1,2);
plot(ti,b,'b--','LineWidth',2);
xlabel('Inversion Time/s');
ylabel('signal phase');
title('signal phase as functiion of TI');

print('Lab3p3_questionB','-dpng');

%% Question C:
% Fitting the data into curve:
[xData, yData] = prepareCurveData( ti, a );

% Set up fittype and options.
ft = fittype( '1-2*exp(-1/b*x)', 'independent', 'x', 'dependent', 'y' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = 0.492723269792208;

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );

% Plot fit with data.
figure( 'Name', '1-2*exp(-t/T1)' );
h = plot( fitresult, xData, yData );
legend( h, 'a vs. ti', '1-2*exp(-t/T1)', 'Location', 'NorthEast' );
title('Inversion Recovery to Measure T1');
% Label axes
xlabel ti/s
ylabel Mxy/T
grid on

print('Lab3p3_questionC_fitting','-dpng');

% As calculated by the curve fitting tool, the TI is 0.859s, very close to
% the ground truth.

%% Question D:

TI = [0.2; 0.8; 3.2];

area = 0;
FA = pi;
for i = 1:400
    rfPulseI(i) = sinc(1250*(i-200)*10^-5);
    area = area + rfPulseI(i)*dt; 
end
sinc_180_amp = FA/(2*pi*gamma*area);
rfPulseI = rfPulseI*sinc_180_amp;

area2 = 0;
FA2 = 0.25*pi;

% 45 degree excitation pulse calculation and design:
% T = 0.002s, TBWP = 3, therefore BW = 1500Hz, t0 = 1/1500s,1/t0 = 1500;
for i = 401:600
    rfPulseE(i) = sinc(1500*(i-500)*10^-5);
    area2 = area2 + rfPulseE(i)*dt; 
end

sinc_45_amp = FA2/(2*pi*gamma*area2);
rfPulseE = rfPulseE*sinc_45_amp;

% The inversion pulse is generated, next, we need to know the index of 45 degree 
% pulse because it is changing with respect to the TI.

for j = 1:length(TI)
    
    for i =1:nTimeSteps   % Reset RF Pulse to zero after each TI loop
        rfPulse(i) = 0;
    end
    
    mT = 0;
    mZ = 1;
    
    for i = 1:400
        rfPulse(i) = sinc_180_amp*sinc(1250*(i-200)*10^-5);
    end
    
    for i = (10^5*TI(j)+100):(10^5*TI(j)+300)
        rfPulse(i) = sinc_45_amp*sinc(1500*(i-(10^5*TI(j)+200))*10^-5);
    end
    
    for i = 1:(10^5*TI(j)+500)
        
    [mT,mZ] =  bloch(dt,0,rfPulse(i),0.85,0.06,mT,mZ,1);
    
    end 
    mFinalVect2(j,:) = [mT, mZ]+(0.02*rand-0.01);
end

c = imag(mFinalVect2(:,1));
d = angle(mFinalVect2(:,1));

clf;
subplot(2,1,1);
plot(TI,c,'+','LineWidth',2);
xlabel('Inversion Time/s');
ylabel('signal amplitude');
title('signal amplitude as functiion of TI with noise');

subplot(2,1,2);
plot(TI,d,'b--','LineWidth',2);
xlabel('Inversion Time/s');
ylabel('signal phase');
title('signal phase as functiion of TI with noise');

print('Lab3p3_questionD_3TI','-dpng');

% Try running the code several times without printing the plot.
% The TI is not as accurate as the previous one, but it has similar
% solution