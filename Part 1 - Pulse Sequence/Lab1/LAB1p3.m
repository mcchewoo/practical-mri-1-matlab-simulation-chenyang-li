    %% Exercise 1.3
clear all; clc; close all; %clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same

%Question B:load the RF wave form saved during exercise 1.2.
load('Pulse1.mat');

% Question C:
% evaluate the magnetization as a function of time
m    = [0,0,0,0,0,0,0,0,0,0,0,0];         %Allocate memory for answer
time = [0,1,2,3,4,5,6,7,8,9,10,11]*10^-7; %Time points for plotting

% using for loop:

m = zeros(1,size(rfPulse,2));
m(1) = 0;
for j = 2:length(rfPulse)
    m(j) = smalltipangle(0,rfPulse(j-1),m(j-1));
end

%Question D: load the pre-simulated data
load('LabSim.mat');

%Question E:Validate your code by plotting both your simulation results 
% and the pre-simulated results in one plot.

x = time;
y1 = abs(m);
y2 = labSim;
plot(x,y1,'r--*',x,y2,'b');
xlabel('time/s');ylabel('Mxy/Tesla');
title('Comparison between Calculation and Simulation of Mxy')
legend('calculated','simulated','Location','Southeast');

print('comparision_Lab1p3','-dpng');
