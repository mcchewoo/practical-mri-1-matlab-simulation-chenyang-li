%% Exercise 1.6
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same

%Allocate the memory needed
rfPulse     = zeros(1,15000); %variable to hold a RF waveform with 10000 samples
gradAmp     = zeros(1,15000); %variable to hold a gradient waveform with 10000 samples
time        = zeros(1,15000); %variable to hold 10000 time points


posZ       = zeros(1,400); %variable to hold 400 positions allong the z direction
mFinal     = zeros(1,400); %variable to hold the final magnetization calculated for each position

dB0AtPosJ  = zeros(1,400);

%Generates the time line for plotting:

for i=1:15000 %i starts at 1 go's to 15000
    time(i)    = i*10^-7;                       %Time in seconds
end

%Generate a list of sampling points along the z direction
for i=1:400 %i starts at 1 go's to 200
    posZ(i)  = (i-200)*10^-4; %Distance from iso center in meters
end  

%Question A: generate a sinc pulse and gradient for 1cm thick slice
for i = 1:10000
    rfPulse(i) = sinc(6000*(i-5000)*10^-7);
for i = 10001:15000
    rfPulse(i) = 0;
end
end

% based on the information, there are two side lobes for the sinc pulse,
% therefore the t0 should be 1/6 ms, and the bandwidth is 6000Hz. Based on
% equation deltaf(2*pi*6000Hz) = gamma(42.577 * 10^6 Hz/T) * Gz * deltaz(0.01m), 
% the Gz should be 8.8 *10^-2 T/m

for i = 1:10000
    gradAmp(i) = 8.8 * 10^-2; %T/m
for i = 10001:15000
    gradAmp(i) = -8.8 * 10^-2; 
end
end

figure(1);
subplot(2,1,1);
plot(time,rfPulse,'Linewidth',3);
xlabel('time/s');
ylabel('B1+/T');
title('RF Waveform');
legend({'x=time'}, 'Location', 'southeast');

subplot(2,1,2);
plot(time,gradAmp,'Linewidth',3);
xlabel('time/s');
ylabel('slice select gradient T/m');
title('slice selective gradient');
legend({'x=time'}, 'Location', 'southeast');

print('Lab1p6_QuestionA','-dpng');

%Question B: 

for j=1:400 %loop over different positions allong the z direction 
    %calculate delta b0 at position j and the 1st time step
    dB0AtPosJ(j) = (j-200)*10^-4 * 8.8 * 10^-2;   % different position of dB0 (400 samples)
    m(j) =  smalltipangle(dB0AtPosJ(j),rfPulse(1), 0); 
    % first time point assuming the transverse magnetization 0
    % first time transverse magnetization all j position
    for i=2:10000 %i starts at 2
        dB0AtPosJ(j) = (j-200)*10^-4 * 8.8 * 10^-2;%update dB0AtPosJ for the ith time step
        m(j) = smalltipangle(dB0AtPosJ(j),rfPulse(i-1),m(j));%update m
    end
    for i = 10001:15000
        dB0AtPosJ(j) = (j-200)*10^-4 * 8.8 * 10^-2*(-1);
        m(j) = smalltipangle(dB0AtPosJ(j),0,m(j));    
    end

    mFinal(j) = m(j); %store final m for each j
end


figure(2)
subplot(2,1,1);
title('magnitude of MR signal');
xlabel('position along z direction/m');
ylabel('magnitude of MR signal')
x1 = posZ;
y4 = abs(mFinal);
plot(x1,y4,'Linewidth',3);
legend({'MR signal magnitude'}, 'Location', 'southeast');

subplot(2,1,2);
title('phase of MR signal');
xlabel('position along z direction/m');
ylabel('phase of MR signal');
y5 = angle(mFinal);
plot(x1,y5,'Linewidth',3);
ylim([-pi pi]);
legend({'MR signal phase'}, 'Location', 'southeast');

print('Lab1p6_QuestionB','-dpng');

% It is not a perfect profile but is better than the previsou one because of 
% the line is not so smooth that may results in artefacts at the edge of the 
% slice profile. Because this slice profile is done without apodization.

%Question C:Save the results in a separate file

save sincb4hann.mat y4 posZ;


