%% Exercise 1.4
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same

%Allocate the memory needed
rfPulse     = zeros(1,10000); %variable to hold a RF waveform with 10000 samples
gradAmp     = zeros(1,10000); %variable to hold a gradient waveform with 10000 samples
time        = zeros(1,10000); %variable to hold 10000 time points

posZ       = zeros(1,400); %variable to hold 400 positions allong the z direction
mFinal     = zeros(1,400); %variable to hold the final magnetization calculated for each position

dB0AtPosJ  = zeros(1,400); % klunge

% Generate the RF waveform (also generates the time line for plotting)
for i=1:10000 %i starts at 1 go's to 10000
    rfPulse(i) = exp(-((i-5000)/2000)^2)*10^-5; %B1+ in Tesla
    gradAmp(i) = 5*10^-2;                       %Tesla per meter
    time(i)    = i*10^-7;                       %Time in seconds
    
end

% Generate a list of sampling points allong the z direction
for i=1:400 %i starts at 1 go's to 200
    posZ(i)  = (i-200)*10^-4; %Distance from iso center in meters
end  

% Question A: (plot the wave form)
clf;
subplot(3,1,1);
x = time;
y1 = abs(rfPulse);
plot(x,y1,'DisplayName','RF amplitude','Linewidth',2);
title('plot of time against RF amplitude');
xlabel('time/s');ylabel('RF amplitude/T');
legend({'RF amplitude'}, 'Location', 'southeast');


subplot(3,1,2);
y2 = angle(rfPulse);
plot(x,y2,'DisplayName','RF phase','Linewidth',2);
title('plot of time against RF phase');
xlabel('time/s');ylabel('RF phase');
legend({'RF pulse'}, 'Location', 'southeast');


subplot(3,1,3);
y3 = gradAmp;
plot(x,y3,'DisplayName','gradient amplitude','Linewidth',2);
title('plot of time against gradient amplitude');
xlabel('time/s');ylabel('gradient amplitude/(T/m)');
legend({'gradient amplitude'}, 'Location', 'southeast');

print('Lab1p4_QuestionA','-dpng');

%Question B: (compute the magnetization / time)

for j=1:400 %loop over different positions allong the z direction 
    %calculate delta b0 at position j and the 1st time step
    dB0AtPosJ(j) = (j-200)*10^-4 * 5*10^-2;   % different position of dB0 (400 samples)
    m(j) =  smalltipangle(dB0AtPosJ(j),rfPulse(1), 0); 
    % first time point assuming the transverse magnetization 0
    % first time transverse magnetization all j position
    for i=2:10000 %i starts at 2
        dB0AtPosJ(j) = (j-200)*10^-4 * 5*10^-2;%update dB0AtPosJ for the ith time step
        m(j) = smalltipangle(dB0AtPosJ(j),rfPulse(i-1),m(j));%update m
    end
    mFinal(j) = m(j); %store final m for each j
end

%Question C: Plot the slice profile
clf;
subplot(2,1,1);
title('magnitude of MR signal');
xlabel('position along z direction/m');
ylabel('magnitude of MR signal')
x1 = posZ;
y4 = abs(mFinal);
plot(x1,y4,'Linewidth',2);
legend({'MR signal magnitude'}, 'Location', 'southeast');

subplot(2,1,2);
title('phase of MR signal');
xlabel('position along z direction/m');
ylabel('phase of MR signal');
y5 = angle(mFinal);
ylim([-pi pi]);
plot(x1,y5,'Linewidth',2);
legend({'MR signal phase'}, 'Location', 'southeast');

print('Lab1p4_QuestionC','-dpng');

%Question D:
%How much signal would this excitation produce? 
%Is this what you would have expected based on the plotted slice profile.

signal = abs(sum(mFinal)); %signal summed over all spins

% The signal is only 0.0164 based on the sum of the magnitude of MR
% signal, which is much lower than the plotted one, the main reason
% for this is that dephasing happens (fanning out). It will cause signal loss by
% loss of net transverse magnetization. (As we can observe from the phase plot)
