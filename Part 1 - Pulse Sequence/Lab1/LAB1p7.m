%% Exercise 1.7
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same

%Allocate the memory needed
rfPulse     = zeros(1,15000); %variable to hold a RF waveform with 10000 samples
gradAmp     = zeros(1,15000); %variable to hold a gradient waveform with 10000 samples
time        = zeros(1,15000); %variable to hold 10000 time points


posZ       = zeros(1,400); %variable to hold 400 positions allong the z direction
mFinal     = zeros(1,400); %variable to hold the final magnetization calculated for each position


%Generates the time line for plotting
for i=1:15000 %i starts at 1 go's to 15000
    time(i)    = i*10^-7;                       %Time in seconds
end

%Generate a list of sampling points allong the z direction
for i=1:400 %i starts at 1 go's to 200
    posZ(i)  = (i-200)*10^-4; %Distance from iso center in meters
end  

%Question A:add han window to the sinc pulse to apodize it
hanning_window = hann(10000)';
for i = 1:10000
    rfPulse(i) = hanning_window(i)*sinc(6000*(i-5000)*10^-7);
for i = 10001:15000
    rfPulse(i) = 0;
end
end

for i = 1:10000
    gradAmp(i) = 8.8 * 10^-2; %T/m
for i = 10001:15000
    gradAmp(i) = -8.8 * 10^-2; 
end
end

%Question B: Plot the wave forms

figure(1);
subplot(2,1,1);
plot(time,rfPulse,'Linewidth',2);
xlabel('time/s');
ylabel('B1+/T');
title('RF Waveform');
legend({'x=time'}, 'Location', 'southeast');

subplot(2,1,2);
plot(time,gradAmp,'Linewidth',2);
xlabel('time/s');
ylabel('slice select gradient T/m');
title('slice selective gradient');
legend({'x=time'}, 'Location', 'southeast');

print('lab1p7_QuestionB','-dpng');

%Question C:
for j=1:400 %loop over different positions allong the z direction 
    %calculate delta b0 at position j and the 1st time step
    dB0AtPosJ(j) = (j-200)*10^-4 * 8.8 * 10^-2;   % different position of dB0 (400 samples)
    m(j) =  smalltipangle(dB0AtPosJ(j),rfPulse(1), 0); 
    % first time point assuming the transverse magnetization 0
    % first time transverse magnetization all j position
    for i=2:10000 %i starts at 2
        dB0AtPosJ(j) = (j-200)*10^-4 * 8.8 * 10^-2;%update dB0AtPosJ for the ith time step
        m(j) = smalltipangle(dB0AtPosJ(j),rfPulse(i-1),m(j));%update m
    end
    for i = 10001:15000
        dB0AtPosJ(j) = (j-200)*10^-4 * 8.8 * 10^-2*(-1);
        m(j) = smalltipangle(dB0AtPosJ(j),0,m(j));    
    end

    mFinal(j) = m(j); %store final m for each j
end

load('sincb4hann');
figure(2);
y5 = abs(mFinal);
plot(posZ,y4,'r--',posZ,y5,'b');
xlabel('position along z direction');
ylabel('magnitude of MR signal');
title('Comparison between apodization and no apodization');
legend('no apodization','apodization by hann function','Location','Southeast');

print('lab1p7_QuestionC','-dpng');
% The slice profile looks different because the apodization makes the slice
% profile looks better.

%Question D:

% thickness is 1 cm, the bandwidth is 6000Hz. It is better now and to make it
% better we can increase the lobe of the sinc pulse (not to truncate it). 

% Question E:
% First, we need to know the frequency offset deltaf:
dz = 0.01; % unit in meters
deltaz = -0.01;
df = 42.577*10^6*0.088*0.01/(2*pi);
deltaf = df*deltaz/dz;
for i = 1:10000
    rfPulse_shift(i) = hanning_window(i)*sinc(6000*(i-5000)*10^-7)*exp(1i*2*pi*deltaf*(i-5000)*10^-7);
end

for i =10001:15000
    rfPulse_shift(i) = 0;
end
for j=1:400 %loop over different positions allong the z direction 
    %calculate delta b0 at position j and the 1st time step
    dB0AtPosJ(j) = (j-200)*10^-4 * 8.8 * 10^-2;   % different position of dB0 (400 samples)
    m(j) =  smalltipangle(dB0AtPosJ(j),rfPulse_shift(1), 0); 
    % first time point assuming the transverse magnetization 0
    % first time transverse magnetization all j position
    for i=2:10000 %i starts at 2
        dB0AtPosJ(j) = (j-200)*10^-4 * 8.8 * 10^-2;%update dB0AtPosJ for the ith time step
        m(j) = smalltipangle(dB0AtPosJ(j),rfPulse_shift(i-1),m(j));%update m
    end
    for i = 10001:15000
        dB0AtPosJ(j) = (j-200)*10^-4 * 8.8 * 10^-2*(-1);
        m(j) = smalltipangle(dB0AtPosJ(j),0,m(j));    
    end

    mFinal_shift(j) = m(j); %store final m for each j
end

% RF waveform plot:
subplot(2,1,1);
plot(time,rfPulse_shift,'Linewidth',3);
xlabel('time/s');
ylabel('B1+/T');
title('RF Waveform');
legend({'x=time'}, 'Location', 'southeast');

subplot(2,1,2);
plot(time,gradAmp,'Linewidth',3);
xlabel('time/s');
ylabel('slice select gradient T/m');
title('slice selective gradient');
legend({'x=time'}, 'Location', 'southeast');

print('Lab1p7_QuestionE_RFWaveform','-dpng');

% Slice profile after shifting 1 cm away from isocenter:
subplot(2,1,1);
title('Slice Profile');
xlabel('position along z direction/m');
ylabel('magnitude of MR signal');
plot(posZ,abs(mFinal_shift),'Linewidth',2);
legend({'MR signal magnitude'}, 'Location', 'southeast');

subplot(2,1,2);
title('phase of MR signal');
xlabel('position along z direction/m');
ylabel('phase of MR signal');
plot(posZ,angle(mFinal_shift),'Linewidth',2);
ylim([-pi pi]);
legend({'MR signal phase'}, 'Location', 'southeast');

print('Lab1p7_QuestionE_shifting_slice','-dpng');

% Answer: The reason for the slice looks a little asymmetric is because of the
% gradient non linearity and by different magnetic susceptibility which
% will cause non-uniformed gradient.

% Question F: 
dz = 0.01; % unit in meters
deltaz = -0.01;
df = 42.577*10^6*0.088*0.01/(2*pi);
deltaf1 = df*deltaz/dz;
deltaf2 = -deltaf1;

for i = 1:10000
    shift_factor1 = exp(1i*(2*pi*deltaf1*(i-5000)*10^-7+pi));
    shift_factor2 = exp(1i*2*pi*deltaf2*(i-5000)*10^-7);
    rfPulse_shift2(i) = hanning_window(i)*sinc(6000*(i-5000)*10^-7)*(shift_factor1 + shift_factor2);
end

for i =10001:15000
    rfPulse_shift2(i) = 0;
end

for j=1:400 %loop over different positions allong the z direction 
    %calculate delta b0 at position j and the 1st time step
    dB0AtPosJ(j) = (j-200)*10^-4 * 8.8 * 10^-2;   % different position of dB0 (400 samples)
    m(j) =  smalltipangle(dB0AtPosJ(j),rfPulse_shift2(1), 0); 
    % first time point assuming the transverse magnetization 0
    % first time transverse magnetization all j position
    for i=2:10000 %i starts at 2
        dB0AtPosJ(j) = (j-200)*10^-4 * 8.8 * 10^-2;%update dB0AtPosJ for the ith time step
        m(j) = smalltipangle(dB0AtPosJ(j),rfPulse_shift2(i-1),m(j));%update m
    end
    for i = 10001:15000
        dB0AtPosJ(j) = (j-200)*10^-4 * 8.8 * 10^-2*(-1);
        m(j) = smalltipangle(dB0AtPosJ(j),0,m(j));    
    end

    mFinal_shift2(j) = m(j); %store final m for each j
end
% Simultaneous multislice RF waveform and phase:
subplot(2,1,1);
plot(time,rfPulse_shift,'Linewidth',3);
xlabel('time/s');
ylabel('B1+/T');
title('RF Waveform');
legend({'x=time'}, 'Location', 'southeast');

subplot(2,1,2);
plot(time,gradAmp,'Linewidth',3);
xlabel('time/s');
ylabel('slice select gradient T/m');
title('slice selective gradient');
legend({'x=time'}, 'Location', 'southeast');
print('Lab1p7_QuestionF_SMS_RFWaveform','-dpng');

% Simultaneous multislice slice profile:
subplot(2,1,1);
title('Slice Profile');
xlabel('position along z direction/m');
ylabel('magnitude of MR signal');
plot(posZ,abs(mFinal_shift2),'Linewidth',2);
legend({'MR signal magnitude'}, 'Location', 'southeast');

subplot(2,1,2);
title('phase of MR signal');
xlabel('position along z direction/m');
ylabel('phase of MR signal');
plot(posZ,angle(mFinal_shift2),'Linewidth',2);
ylim([-pi pi]);
legend({'MR signal phase'}, 'Location', 'southeast');

print('Lab1p7_QuestionF_SMS','-dpng');