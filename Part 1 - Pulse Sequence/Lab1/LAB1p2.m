%% Exercise 1.2
clear all; clc; close all; %clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same


rfPulse      = [ 0,0.5, 1, 1, 1, 1, 1, 1, 1, 1, 0.5, 0]*10^-6; %B1+  in Tesla
timeLine     = [ 0,  1, 2, 3, 4, 5, 6, 7, 8, 9,  10,11]*10^-7; %Time in seconds

%Question A: plot the RF waveform

plot(timeLine,rfPulse,'b','Linewidth',2);
xlabel('time/s');
ylabel('B1+/T');
title('RF Waveform');
legend('time', 'Location', 'southeast');

print('RF_waveform_Lab1p2A','-dpng');

%Question B:What is the duration of the RF pulse? Do you think the MR system 
%could accurately produce this shape? Write your answer (and explanation) 

% Answer: The duration of the pulse would be 7*10^-7 seconds. No, it is the
% ideal case that the system will generate a gradient as calculated, in
% practice, for example, eddy current will spoil the perfect gradient and
% make the gradient look a little tilted and not as linearly as we want.

%Question C: Save the RF waveform to disk such that it can be loaded again 
% by subsequent MATLAB programs

save Pulse1.mat rfPulse timeLine
