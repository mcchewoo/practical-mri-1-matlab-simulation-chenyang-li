%% Exercise 1.5
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same

%Allocate the memory needed
rfPulse     = zeros(1,15000); %variable to hold a RF waveform with 10000 samples
gradAmp     = zeros(1,15000); %variable to hold a gradient waveform with 10000 samples
time        = zeros(1,15000); %variable to hold 10000 time points


posZ       = zeros(1,400); %variable to hold 400 positions allong the z direction
mFinal     = zeros(1,400); %variable to hold the final magnetization calculated for each position

dB0AtPosJ  = zeros(1,400);  
%Generate the RF waveform
for i=1:10000 %i starts at 1 go's to 10000
    rfPulse(i) = exp(-((i-5000)/2000)^2)*10^-5; %B1+ in Tesla
    gradAmp(i) = 5*10^-2;                       %Tesla per meter
for i = 10001:15000
    rfPulse(i) = 0;
    gradAmp(i) = -5*10^-2;
end
end

%Generates the time line for plotting
for i=1:15000 %i starts at 1 go's to 15000
    time(i)    = i*10^-7;                       %Time in seconds
end

%Generate a list of sampling points allong the z direction
for i=1:400 %i starts at 1 go's to 200
    posZ(i)  = (i-200)*10^-4; %Distance from iso center in meters
end  

%Question A: add a refocusing gradient, the area of refocusing gradient
%should be half area as of the gradient.
for i = 10001:15000
    rfPulse(i) = 0;
    gradAmp(i) = -5*10^-2;
end
%Question B:plot in one single figure, the RF amplitude, RF phase and gradient
% amplitude as a function of time 
figure(1);
subplot(3,1,1);
x = time;
y1 = abs(rfPulse);
plot(x,y1,'DisplayName','RF amplitude','Linewidth',3);
title('plot of time against RF amplitude');
xlabel('time/s');ylabel('RF amplitude/T');
legend({'RF amplitude'}, 'Location', 'southeast');

subplot(3,1,2);
y2 = angle(rfPulse);
plot(x,y2,'DisplayName','RF phase','Linewidth',3);
title('plot of time against RF phase');
xlabel('time/s');ylabel('RF phase');
legend({'RF phase'}, 'Location', 'southeast');

subplot(3,1,3);
y3 = gradAmp;
plot(x,y3,'DisplayName','gradient amplitude','Linewidth',3);
title('plot of time against gradient amplitude')
xlabel('time/s');ylabel('gradient amplitude/(T/m)');
legend({'gradient amplitude'}, 'Location', 'southeast');

print('Lab1p5_QuestionB','-dpng');

%Question C: simulate the slice profile

for j=1:400 %loop over different positions allong the z direction 
    %calculate delta b0 at position j and the 1st time step
    dB0AtPosJ(j) = (j-200)*10^-4 * 5*10^-2;   % different position of dB0 (400 samples)
    m(j) =  smalltipangle(dB0AtPosJ(j),rfPulse(1), 0); 
    % first time point assuming the transverse magnetization 0
    % first time transverse magnetization all j position
    for i=2:10000 %i starts at 2
        dB0AtPosJ(j) = (j-200)*10^-4 * 5*10^-2;%update dB0AtPosJ for the ith time step
        m(j) = smalltipangle(dB0AtPosJ(j),rfPulse(i-1),m(j));%update m
    end
    for i = 10001:15000
        dB0AtPosJ(j) = (j-200)*10^-4 * 5*10^-2*(-1);
        m(j) = smalltipangle(dB0AtPosJ(j),0,m(j));    
    end

    mFinal(j) = m(j); %store final m for each j
end


%Question D:Plot in a single figure (two sub plots underneath one another) the magnitude and phase of
%the MR signal as a function of the position along the z direction

figure(2)
subplot(2,1,1);
title('magnitude of MR signal');
xlabel('position along z direction/m');
ylabel('magnitude of MR signal')
x1 = posZ;
y4 = abs(mFinal);
plot(x1,y4,'Linewidth',3);
legend({'MR signal magnitude'}, 'Location', 'southeast');

subplot(2,1,2);
title('phase of MR signal');
xlabel('position along z direction/m');
ylabel('phase of MR signal');
y5 = angle(mFinal);
ylim([-pi pi]);
plot(x1,y5,'Linewidth',3);
legend({'MR signal phase'}, 'Location', 'southeast');

print('Lab1p5_QuestionD','-dpng');

% Question E:How much signal does the excitation
% produce now? Why is there more signal now than in Exercise 1.4?
signal = abs(sum(mFinal));
% The total signal is 12.581, which is larger than the previous signal
% without refocusing gradient.Becuase the refoucsing gradient refocus the 
% magnetization by rephrasing,thus the loss signal could be restored 
% and we can see from the plot that the phase is same within the slice. 

% Question F:
% The slice thickness would be 1cm.(FWHM) as seen from the plot
% This slice is not a good one becuse the transverse magnetization
% (signal source) is not uniform (a hump), therefore there will be 
% some artefacts at the edge of the slice which is away for isocenter.
