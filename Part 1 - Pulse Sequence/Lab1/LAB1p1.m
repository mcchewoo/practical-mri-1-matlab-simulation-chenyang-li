%% Exercise 1.1
clear all; clc; close all; %clean up

%Question A:

% Modify the existing code to calculate how much mz changes as a function of the flip-angle

flipAngle = 1; %flip angle in degrees.

cos(pi*flipAngle/180); %example how to use the cosine fuction in matlab

deltaMz = 1 - cos(pi*flipAngle/180);% how much mz changed 

% The Mz has a very small change (1.523e-04);

%Question B:

%Based on your findings, what do you think will be the maximum flip-angle for which
%this approximation is valid?

% Anser: the maximum flip angle for small flip angle approximation would be around
% 30 degree, after calculation, the change of the longitudinal magnetization
% is only 0.134, which can be seen as 

deltaMzmax = 1 - cos(pi*30/180); % how much mz changed with the largest small tip angle

% as we see from the result, the deltaMzmax is still quite small in an
% acceptable range for samll tip angle approximation (0.1340)