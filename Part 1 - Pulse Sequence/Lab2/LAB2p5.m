%% Exercise 2.5
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same

dt    = 10^-7; 
gamma = 42.577*10^6;

%Allocate the memory needed
nTimeSteps  = 60000;
rfPulse2    = zeros(1,nTimeSteps); %variable to hold a RF waveform with 30000 samples
rfPulse    = zeros(1,nTimeSteps);
rfPulse1     = zeros(1,nTimeSteps); %variable to hold a RF waveform with 30000 samples
rfPulse3     = zeros(1,nTimeSteps);
gradAmp     = zeros(1,nTimeSteps); %variable to hold a gradient waveform with 30000 samples
time        = zeros(1,nTimeSteps); %variable to hold 30000 time points

nPosSteps  = 200;
posZ       = zeros(1,nPosSteps); %variable to hold 400 positions allong the z direction
mFinalVect = zeros(nPosSteps,2); %variable to hold the final magnetization calculated for each position

for i = 1:nPosSteps
    posZ(i) = (i-100)*10^-4; % from -1 to 1cm in steps of 0.01cm.
end

%Generates the time line for plotting
for i=1:nTimeSteps %i starts at 1 go's to 15000
    time(i)    = i*10^-7;                       %Time in seconds
end

% For 45 degree pulse:
% Duration = 0.002s, TBWP = 2, dz = 0.005m. Therefore, the BW(df) = 1000Hz
% Gz = df/(gamma*dz) = 1000/(42.57*10^6*0.005) = 0.0047 T/m 

% Question A:

% Creating 180 degree inversion pulse and it gradient amplitude:
% For 180 degree inversion pulse: 
% Duration = 0.002s, TBWP = 3, dz = 0.01m. Therefore, the BW(df) = 1500Hz
% Gz = 2*pi*df/(gamma*dz) = 1500/(42.577*10^6*0.01) = 2*pi*0.0035T/m, t0 = 0.002/3,
% therefore 1/t0 = 1500
area = 0;
FA = pi;
hanning_window1 = hann(20000);
for i = 1:20000
    rfPulse1(i) = hanning_window1(i)*sinc(1500*(i-10000)*10^-7);
    area = area + rfPulse1(i)*dt;
    gradAmp(i) = 0.0035;
end
    
sinc_180_amp = FA/(2*pi*gamma*area);
rfPulse1 = rfPulse1*sinc_180_amp;

Valid_FA180 = 0;
for i = 1:20000
 Valid_FA180 = Valid_FA180 + gamma*2*pi*rfPulse1(i)*dt;
end
% This is to test if get the right RF Pulse, answer is 3.14(pi). Correct!

for i = 20001:40000
    rfPulse2(i) = 0;
    gradAmp(i) = 0.045; % Using the maximum gradient amplitude to let the 
    % spin dephase as much as possible, which is known as rapid phase
    % oscillaton
end

%Creating 45 degree excitation pulse:
% For 45 degree pulse:
% Duration = 0.002s, TBWP = 2, dz = 0.005m. Therefore, the BW(df) = 1000Hz
% Gz = df/(gamma*dz) = 1000/(42.57*10^6*0.005) = 0.0047 T/m, t0 = 0.002/2,
% therefore 1/t0 = 1000;

area2 =0;
FA2 = 0.25*pi;
hanning_window2 = hann(20000);
for i = 40001:60000
    rfPulse3(i) = hanning_window2(i-40000)*sinc(1000*(i-50000)*10^-7);
    area2 = area2 + rfPulse3(i)*dt;
    gradAmp(i) = 0.0047;
end

sinc_45_amp = FA2/(2*pi*gamma*area2);
rfPulse3 = rfPulse3*sinc_45_amp;

Valid_FA_45 = 0;
for i = 40001:60000
 Valid_FA_45 = Valid_FA_45 + gamma*2*pi*rfPulse3(i)*dt;
end
% This is to test if get the right RF Pulse, answer is 0.7854(pi/4). Correct!

rfPulse = rfPulse1 + rfPulse2+ rfPulse3;

% Simulate slice profile:
tic;
for j=1:nPosSteps

    mT = 0;
    mZ = 1;
    %update dB0AtPosJ:
    %dB0AtPosJ = ....
    dB0AtPosJ = posZ(j)*gradAmp(1);
    % start large tip angle starting from [0,0,1]'
    % [mT,mZ] = fastsim(....
    [mT, mZ] = fastsim_inv(dB0AtPosJ, rfPulse(1), mT, mZ);

    for i=2:nTimeSteps %i starts at 2
    	%update dB0AtPosJ:
    	%dB0AtPosJ = ....
    	dB0AtPosJ = posZ(j)*gradAmp(i);
    	[mT, mZ] = fastsim_inv(dB0AtPosJ, rfPulse(i), mT, mZ);
    
    	% start large tip angle starting from [0,0,1]'
    	% [mT,mZ] = fastsim(....
    end
    mFinalVect(j,:) = [mT, mZ]; %m(1)+1i*m(2);
end
toc;

displaysequence(time,rfPulse,gradAmp,'Lab2p5QuestionA')

% Question B:
subplot(3,1,1);
plot(posZ, abs(mFinalVect(:,1)),'LineWidth',2);
title('xy magnitude');
ylabel('magnitude');
	
subplot(3,1,2);
plot(posZ, angle(mFinalVect(:,1)),'LineWidth',2);
title('xy phase');
ylabel('Phase');

subplot(3,1,3);
plot(posZ, mFinalVect(:,2),'LineWidth',2);
title('z magnitude');
ylabel('magnitude');
xlabel('z Position');

print('Lab2p5_QuestionB_inv_45_thickness0.01','-dpng');
% Answer: No, But the inversion is getting better that in the plane it is
% getting inverted in some ways. This may a problem for slice selection but
% for inversion recovery, it would be the problem, since inversion is used
% to enhance T1 contrast with different tissue. In real world, it is
% difficult to invert all the spins uniformly.

%% Question C:
% (1) slice thickness of 2cm (0.02m):
% Duration = 0.002s, TBWP = 3, dz = 0.02m. Therefore, the BW(df) = 1500Hz
% Gz = df/(gamma*dz) = 1500/(42.577*10^6*0.02) = 0.0018T/m, t0 = 0.002/3,
% therefore 1/t0 = 1500

area = 0;
FA = pi;
hanning_window1 = hann(20000);
for i = 1:20000
    rfPulse1(i) = hanning_window1(i)*sinc(1500*(i-10000)*10^-7);
    area = area + rfPulse1(i)*dt;
    gradAmp(i) = 0.0018;
end
    
sinc_180_amp = FA/(2*pi*gamma*area);
rfPulse1 = rfPulse1*sinc_180_amp;

for i = 20001:40000
    rfPulse2(i) = 0;
    gradAmp(i) = 0.045; % Using the maximum gradient amplitude to let the 
    % spin dephase as much as possible, which is known as rapid phase
    % oscillaton
end

area2 =0;
FA2 = 0.25*pi;
hanning_window2 = hann(20000);
for i = 40001:60000
    rfPulse3(i) = hanning_window2(i-40000)*sinc(1000*(i-50000)*10^-7);
    area2 = area2 + rfPulse3(i)*dt;
    gradAmp(i) = 0.5*0.0047;
end

sinc_45_amp = FA2/(2*pi*gamma*area2);
rfPulse3 = rfPulse3*sinc_45_amp;

rfPulse = rfPulse1 + rfPulse2+ rfPulse3;

for j=1:nPosSteps

    mT = 0;
    mZ = 1;
    %update dB0AtPosJ:
    %dB0AtPosJ = ....
    dB0AtPosJ = posZ(j)*gradAmp(1);
    % start large tip angle starting from [0,0,1]'
    % [mT,mZ] = fastsim(....
    [mT, mZ] = fastsim_inv(dB0AtPosJ, rfPulse(1), mT, mZ);

    for i=2:nTimeSteps %i starts at 2
    	%update dB0AtPosJ:
    	%dB0AtPosJ = ....
    	dB0AtPosJ = posZ(j)*gradAmp(i);
    	[mT, mZ] = fastsim_inv(dB0AtPosJ, rfPulse(i), mT, mZ);
    
    	% start large tip angle starting from [0,0,1]'
    	% [mT,mZ] = fastsim(....
    end
    mFinalVect2(j,:) = [mT, mZ]; %m(1)+1i*m(2);
end

subplot(3,1,1);
plot(posZ, abs(mFinalVect2(:,1)),'LineWidth',2);
title('xy magnitude');
ylabel('magnitude');
	
subplot(3,1,2);
plot(posZ, angle(mFinalVect2(:,1)),'LineWidth',2);
title('xy phase');
ylabel('Phase');

subplot(3,1,3);
plot(posZ, mFinalVect2(:,2),'LineWidth',2);
title('z magnitude');
ylabel('magnitude');
xlabel('z Position');

print('Lab2p5_QuestionB_inv_45_thickness0.02','-dpng');

% By using twice thickness, the inversion get better. And the result
% improves.

% (2) by using hard pulse:
% 180 inversion hard pulse:
% Duration = 0.002s; df = 1/T = 500; Gz = 500/(42.577*10^6*0.01) = 0.0012
T = 0.002;
Amp_hard_pulse_180 = pi/(2*pi*gamma*T); 

for i = 1:20000
    rfPulse(i) = Amp_hard_pulse_180;
    gradAmp(i) = 0.0012;
end

for i = 20001:40000
    rfPulse(i) = 0;
    gradAmp(i) = 0.045;
end

% for 45 degree hard pulse:
% Duration = 0.002s; df = 500,Gz = 500/(42.577*10^6*0.005) = 0.0023;
Amp_hard_pulse_45 = 0.25*pi/(2*pi*gamma*T);
for i = 40001:60000
    rfPulse(i) = Amp_hard_pulse_45;
    gradAmp(i) = 0.0023;
end

clf;
plot(time,rfPulse,'LineWidth',2);% Plotting hard pulse sequence
xlabel('time/s');
ylabel('RF Pulse/T');
title('Hard Pulse waveform');
print('Lab2p5_QuestionC_HardPulse','-dpng');

for j=1:nPosSteps

    mT = 0;
    mZ = 1;
    %update dB0AtPosJ:
    %dB0AtPosJ = ....
    dB0AtPosJ = posZ(j)*gradAmp(1);
    % start large tip angle starting from [0,0,1]'
    % [mT,mZ] = fastsim(....
    [mT, mZ] = fastsim_inv(dB0AtPosJ, rfPulse(1), mT, mZ);

    for i=2:nTimeSteps %i starts at 2
    	%update dB0AtPosJ:
    	%dB0AtPosJ = ....
    	dB0AtPosJ = posZ(j)*gradAmp(i);
    	[mT, mZ] = fastsim_inv(dB0AtPosJ, rfPulse(i), mT, mZ);
    
    	% start large tip angle starting from [0,0,1]'
    	% [mT,mZ] = fastsim(....
    end
    mFinalVect3(j,:) = [mT, mZ]; %m(1)+1i*m(2);
end

figure(3)
subplot(3,1,1);
plot(posZ, abs(mFinalVect(:,1)),'r',posZ, abs(mFinalVect2(:,1)),'b',posZ, abs(mFinalVect3(:,1)),'g');
title('xy magnitude');
ylabel('magnitude');
legend('thickness = 0.01m','thickness = 0.02m','hard pulse');
	
subplot(3,1,2);
plot(posZ, angle(mFinalVect(:,1)),'r', posZ, angle(mFinalVect2(:,1)),'b', posZ,angle(mFinalVect3(:,1)),'g');
title('xy phase');
ylabel('Phase');
legend('thickness = 0.01m','thickness = 0.02m','hard pulse');

subplot(3,1,3);
plot(posZ, mFinalVect(:,2),'r', posZ,mFinalVect2(:,2),'b',posZ,mFinalVect3(:,2),'g');
title('z magnitude');
ylabel('magnitude');
xlabel('z Position');
legend('thickness = 0.01m','thickness = 0.02m','hard pulse');

print('Lab2p5_QuestionC_Comparison_of_three_pulses','-dpng');

%% Question D:
% Let us analyzing mFinalVect2, which stores the signal value of 2 cm
% slice thickness inversion:
signal_out_slice = 0;
signal_in_slice = 0;
for j = 1:nPosSteps
    if j <= 50 || j >= 150
    signal_out_slice = signal_out_slice + abs(mFinalVect2(j,1)); 
    else 
    signal_in_slice = signal_in_slice + abs(mFinalVect2(j,1)); 
    end 
end

signal_pct_in = signal_in_slice/signal_out_slice;

% From this calculation, we know that the signal coming within the slice is
% only 36%, which means 64% of all the signal collected is out of slice.

%Question E:

% Answer: Yes, because the large gradient amplitude will weaken the weight
% of inhomogeneity generated by different spins.

%Question F:
%In the absence of stronger gradients one could make the gradients longer. How long would
%the minimum delay between the inversion and excitation need to be in order to make sure
%that the contamination from out of slice signal is less than 1%?

% Answer: the time is proportional to the area under the gradient. We know
% that 2ms delay gives out 36% of signal. In order to get 99% of signal, we
% can get:

delay_min = 2*99/36;

% Therefore the minimum delay between the inversion and excitation is 5.5
% miliseconds.

