function [ m ] = largetipangle(dB0,B1,m)

    dt    = 10^-7;       %0.1 micro second
    gamma = 42.577*10^6; %MHz per Tesla
    
    B1x = real(B1);
    B1y = imag(B1);
    vec = [0,dB0,-B1y;-dB0,0,B1x;B1y,-B1x,0];
    
    m = m + gamma.*dt.*vec*m;
    
end