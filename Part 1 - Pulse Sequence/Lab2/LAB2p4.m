%% Exercise 2.4
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same

dt    = 10^-7; 
gamma = 42.577*10^6;

%Allocate the memory needed
nTimeSteps  = 30000;
rfPulse2    = zeros(1,nTimeSteps); %variable to hold a RF waveform with 30000 samples
rfPulse     = zeros(1,nTimeSteps); %variable to hold a RF waveform with 30000 samples
gradAmp     = zeros(1,nTimeSteps); %variable to hold a gradient waveform with 30000 samples
time        = zeros(1,nTimeSteps); %variable to hold 30000 time points

nPosSteps  = 200;
posZ       = zeros(1,nPosSteps); %variable to hold 400 positions allong the z direction
mFinalVect = zeros(nPosSteps,2); %variable to hold the final magnetization calculated for each position


%Generates the time line for plotting
for i=1:nTimeSteps %i starts at 1 go's to 15000
    time(i)    = i*10^-7;                       %Time in seconds
end

%Generate a list of sampling points allong the z direction
for i=1:nPosSteps %i starts at 1 go's to 200
    posZ(i)  = (i-100)*10^-4; %Distance from iso center in meters
end  

%Question A:

% Inversion Pulse Sequence Design:
% two important parameters for designing a inversion pulse are 1)
% time-bandwith product (T*df) and 2) flip angle (theta = gamma*B1*T to
% determine A and t0 for sinc pulse.

% 1)In this case, we know that the time-bandwidth product equation: T*df =
% gamma*Gz*slice_thickness. It is known that increasing T*df will improve
% the spatial profile, therefore we use the maximum gradient to get the
% maximum TBWP = 42.577*10^6*0.045*0.01*0.002=38.3193, therefore we can
% approximate that there are 19 zeros crossing to the left and right of the
% central peak according to the equation T*df = NL + NR.In this case, there
% are 18 lobes on both side, therefore, we can know the t0 = 0.002s/38;
% REASON: because higher gradient amplitude gives out higher time-bandwidth
% product, the higher the time-bandwidth product, the better the slice
% profile, therefore, we use the maximum gradient of 0.045T/m to design
% Sinc RF pulse.

% 2) next we need determine the amplitude of the inversion pulse, which is
% closely related to the flip angle, for inversion pulse, the ideal flip
% angle is 180 degree (pi). Using equation theta (FA) = gammaBar *
% Area under the RF envelope. Therefore, the amplitude should be Amp =
% FA/(gamma*area with 1 amplitude sinc pulse);

area = 0; % initiating area
FA = pi;
% normalized pulse with 1 amplitude:
hanning_window = hann(20000)';
for i = 1:20000
    rfPulse(i) = hanning_window(i)*sinc(19160*(i-10000)*10^-7); % Pulse function
    % Calculating the area under the RF Pulse
    gradAmp(i) = 0.045;
    area = area + rfPulse(i)*dt;   
end

%rfPulse = pi*rfPulse/area;
sinc_180_amp = FA/(2*pi*gamma*area);
 
rfPulse = rfPulse*sinc_180_amp;
%rfPulse_180 = sinc_180_amp*rfPulse;

valid_FA = 0;
for i = 1:20000
 valid_FA = valid_FA + gamma*2*pi*rfPulse(i)*dt;
end
% This is to test if get the right RF Pulse, answer is 3.14(pi). Correct!

for i = 20000:30000
    rfPulse(i) = 0;
    gradAmp(i) = -0.045; % refocusing gradient to rephase as much as possible
end 

displaysequence(time,rfPulse,gradAmp,'Lab2p4_QuestionA');

%Question B:
tic;
for j=1:nPosSteps

    mT = 0;
    mZ = 1;
    %update dB0AtPosJ:
    %dB0AtPosJ = ....
    dB0AtPosJ = posZ(j)*gradAmp(1);
    % start large tip angle starting from [0,0,1]'
    % [mT,mZ] = fastsim(....
    [mT, mZ] = fastsim_inv(dB0AtPosJ, rfPulse(1), mT, mZ);

    for i=2:nTimeSteps %i starts at 2
    	%update dB0AtPosJ:
    	%dB0AtPosJ = ....
    	dB0AtPosJ = posZ(j)*gradAmp(i);
    	[mT, mZ] = fastsim_inv(dB0AtPosJ, rfPulse(i), mT, mZ);
    
    	% start large tip angle starting from [0,0,1]'
    	% [mT,mZ] = fastsim(....
    end

    mFinalVect(j,:) = [mT, mZ]; %m(1)+1i*m(2);
end
toc;

subplot(3,1,1);
plot(posZ, abs(mFinalVect(:,1)),'LineWidth',2);
title('xy magnitude');
ylabel('magnitude');
	
subplot(3,1,2);
plot(posZ, angle(mFinalVect(:,1)),'LineWidth',2);
title('xy phase');
ylabel('Phase');

subplot(3,1,3);
plot(posZ, mFinalVect(:,2),'LineWidth',2);
title('z magnitude');
ylabel('magnitude');
xlabel('z Position');

%Question B_2: out of slice, there still is transverse magnetization away
%from isocenter as the diagram shows, which means only the magnetization on
%the isocenter is accurately inverted to 180 degree.

%Question C:
% Ideally, it should be all inverted, but only the inversion pulse cannot
% uniformly invert all the spins. B0 inhomogeneities may contribute to the
% non-uniformly distributed inversion.
%Question D:

% It is not physically possible because small tip angle approximation is
% based on the assumption that the longitudinal magnetization remains
% unchanged, however, for inversion pulse, it is impossible to keep mz same
% as before inversion.
