%% Exercise 2.3
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same

%load sequence
load('sinc_excitation.mat');
nTimeSteps = size(time,2);

%Allocate the memory needed
nPosSteps  = 200;
posZ       = zeros(1,nPosSteps); %variable to hold the positions allong the z direction
mFinalVect = zeros(nPosSteps,2); %variable to hold the final magnetization calculated for each position


%Generate a list of sampling points allong the z direction
for i=1:nPosSteps 
    posZ(i)  = (i-100)*10^-4; %Distance from iso center in meters
end  

%Question A:
displaysequence(time,rfPulse,gradAmp,'Lab2p3_QuestionA');
%Question B:
tic;
for j=1:nPosSteps

    mT = 0;
    mZ = 1;
    %update dB0AtPosJ:
    %dB0AtPosJ = ....
    dB0AtPosJ = posZ(j)*gradAmp(1);
    % start large tip angle starting from [0,0,1]'
    % [mT,mZ] = fastsim(....
    [mT, mZ] = fastsim(dB0AtPosJ, rfPulse(1), mT, mZ);

    for i=2:nTimeSteps %i starts at 2
    	%update dB0AtPosJ:
    	%dB0AtPosJ = ....
    	dB0AtPosJ = posZ(j)*gradAmp(i);
    	[mT, mZ] = fastsim(dB0AtPosJ, rfPulse(i), mT, mZ);
    
    	% start large tip angle starting from [0,0,1]'
    	% [mT,mZ] = fastsim(....
    end

    mFinalVect(j,:) = [mT, mZ]; %m(1)+1i*m(2);
end
toc;
%Question C:
subplot(3,1,1);
plot(posZ, abs(mFinalVect(:,1)),'LineWidth',2);
title('xy magnitude');
ylabel('magnitude');
	
subplot(3,1,2);
plot(posZ, angle(mFinalVect(:,1)),'LineWidth',2);
title('xy phase');
ylabel('Phase');

subplot(3,1,3);
plot(posZ, mFinalVect(:,2),'LineWidth',2);
title('z magnitude');
ylabel('magnitude');
xlabel('z Position');

print('Lab2p3_QuestionC','-dpng');
    
%Question D:
clf;
load('Lab2p1_ref.mat');
subplot(2,1,1);
plot(posZ, abs(mFinalVect(:,1)),'b',posZ,abs(Lab2p1_ref),'r--');
title('xy magnitude comparison');
ylabel('magnitude');
legend('simulation','reference');
	
subplot(2,1,2);
plot(posZ, angle(mFinalVect(:,1)),'b',posZ,angle(Lab2p1_ref),'r--');
title('xy phase');
ylabel('Phase');
legend('simulation','reference');

print('Lab2p3_QuestionD_ref','-dpng');

%Question E:

%Elapsed time is 0.831040 seconds. Way much faster than previous two!
