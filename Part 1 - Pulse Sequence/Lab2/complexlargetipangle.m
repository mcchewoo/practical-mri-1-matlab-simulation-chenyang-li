function [ mOut ] = complexlargetipangle(dB0,B1,m)

    dt    = 10^-7;       %0.1 micro second
    gamma = 42.577*10^6; %MHz per Tesla
    
    mOut  = m; %allocating memory same size as m
	
	mOut(1) = m(1) - (complex(0,1)*gamma*dt).*(dB0.*m(1)+B1*m(2));
	
	mOut(2) = m(2) + (gamma*dt)*(real(B1)*imag(m(1))-imag(B1)*real(m(1)));
	
end