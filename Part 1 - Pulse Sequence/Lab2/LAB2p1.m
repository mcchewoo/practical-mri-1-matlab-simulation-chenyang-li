%% Exercise 2.1
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same

%load sequence
load('sinc_excitation.mat');
nTimeSteps = size(time,2);

%Allocate the memory needed
nPosSteps  = 200;
posZ       = zeros(1,nPosSteps); %variable to hold the positions allong the z direction
mFinal     = zeros(nPosSteps,3); %variable to hold the final magnetization calculated for each position

%Generate a list of sampling points allong the z direction
for i=1:nPosSteps
    posZ(i)  = (i-100)*10^-4; %Distance from iso center in meters
end  

%Question B

displaysequence(time,rfPulse,gradAmp,'Lab2p1_QuestionB');

%Question D:
tic;
for j=1:nPosSteps
    m = [0,0,1]';
    %update dB0AtPosJ:
    dB0AtPosJ = posZ(j)* gradAmp(1);
    % start large tip angle starting from [0,0,1]'
    m = largetipangle(dB0AtPosJ,rfPulse(1),m);
    
    for i=2:nTimeSteps %i starts at 2
        
        dB0AtPosJ = posZ(j) * gradAmp(i);
        m = largetipangle(dB0AtPosJ,rfPulse(i),m);
        
    end

    mFinal(j,:) = m;
   
end
toc;

%Question E:
figure
subplot(3,1,1)
plot(posZ, abs(complex(mFinal(:,1),mFinal(:,2))),'LineWidth',2);
title('Signal Magnitude');
xlabel('Position');
ylabel('Magnitude');

subplot(3,1,2)
plot(posZ, angle(complex(mFinal(:,1),mFinal(:,2))),'LineWidth',2);
title('Signal Phase');
xlabel('Position');
ylabel('Signal Phase');

subplot(3,1,3);
plot(posZ, mFinal(:,3),'LineWidth',2);
title('longitudinal Magnitude');
xlabel('Position');
ylabel('Magnitude');

print('Lab2p1_QuestionE_1','-dpng');

%load file
load('Lab2p1_ref.mat');

subplot(2,1,1);
plot(posZ, abs(complex(mFinal(:,1),mFinal(:,2))),'r--*',posZ,abs(Lab2p1_ref),'b');
title('comparision of magnitude between calculation and reference');
xlabel('Position');
ylabel('Magnitude');

subplot(2,1,2);
plot(posZ,angle(complex(mFinal(:,1),mFinal(:,2))),'r--o',posZ,angle(Lab2p1_ref));
title('comparison of phase between calculation and reference');
xlabel('Position');
ylabel('Phase');

print('Lab2p1_QuestionE_compare','-dpng');

%Question F:
save lab2p1f1.mat mFinal;
tic;
for j=1:nPosSteps
    m = [0,0,1]';
    %update dB0AtPosJ:
    dB0AtPosJ = posZ(j)* gradAmp(1);
    % start large tip angle starting from [0,0,1]'
    m = largetipangle(dB0AtPosJ,20*rfPulse(1),m);
    
    for i=2:nTimeSteps %i starts at 2
        
        dB0AtPosJ = posZ(j) * gradAmp(i);
        m = largetipangle(dB0AtPosJ,20*rfPulse(i),m);
        
    end

    mFinal_20(j,:) = m;
   
end
toc;
save lab2p1f20.mat mFinal_20;

% to normalize the value we need to find the maximum value of the
% magnetization of mFinal and mFinal_20
maxmag_b4 = max(complex(mFinal(:,1),mFinal(:,2)));
maxmag_20 = max(complex(mFinal_20(:,1),mFinal_20(:,2)));

mFinal_norm_b4 = complex(mFinal(:,1),mFinal(:,2))/maxmag_b4;
mFinal_norm_20 = complex(mFinal_20(:,1),mFinal_20(:,2))/maxmag_20;
clf;
plot(posZ,mFinal_norm_b4,'r--*',posZ,mFinal_norm_20,'b');
xlabel('posion/m');
ylabel('magnitude/T');
legend('b4','*20');

print('lab2p1_QuestionF_comparison_20','-dpng');

%The magnitude is different, while after we normalize them into same scale,
%small tip angle can predict the result because of the assumption of small
%tip angle approximation. Small tip angle approximates that the longitudinal magnetization remains
%same, therefore the magnitude of the pulse sequence will not affect
%transverse magnetization after normalization.

%Question G:

% for first one, Elapsed time is 4.168062 seconds.
% for second one,Elapsed time is 4.134359 seconds.