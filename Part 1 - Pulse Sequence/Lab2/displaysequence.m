function displaysequence(time,rfPulse,gradAmp,name)


subplot(3,1,1);
plot(time, abs(rfPulse),'LineWidth',2);
title('RF Amplitude');
xlabel('time/s');
ylabel('amplitude/T');

subplot(3,1,2);
plot(time, angle(rfPulse),'LineWidth',2);
title('RF Phase');
xlabel('time/s');
ylabel('Phase');

subplot(3,1,3);
plot(time, gradAmp,'LineWidth',2);
title('Gradient Amplitude');
xlabel('time/s');
ylabel('T/m');

print(name,'-dpng');
end

