%% Exercise 2.2
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same

%load sequence
load('sinc_excitation.mat');
nTimeSteps = size(time,2);


%Allocate the memory needed
nPosSteps  = 200;
posZ       = zeros(1,nPosSteps); %variable to hold the positions allong the z direction
mFinalVect = zeros(nPosSteps,2); %variable to hold the final magnetization calculated for each position


%Generate a list of sampling points allong the z direction
for i=1:nPosSteps %i starts at 1 go's to 200
    posZ(i)  = (i-100)*10^-4; %Distance from iso center in meters
end  


%Question A:
displaysequence(time,rfPulse,gradAmp,'Lab2p2_QuestionA');

%Question C:

%m = [0,1];
tic;
for j=1:nPosSteps

    m = [0,1];
    %update dB0AtPosJ:
    %dB0AtPosJ = ....
    dB0AtPosJ = posZ(j)*gradAmp(1);
    % start large tip angle starting from [0,0,1]'
    % m = largetipangle(....
    m = complexlargetipangle(dB0AtPosJ, rfPulse(1), m); 
    for i=2:nTimeSteps %i starts at 2
        
        %update dB0AtPosJ:
        %dB0AtPosJ = ....
        dB0AtPosJ = posZ(j)*gradAmp(i);
        %update udpate m:
        %m = largetipangle(... 
	m = complexlargetipangle(dB0AtPosJ, rfPulse(i), m);
    end
    
    mFinalVect(j,:) = m; 
end
toc;

%Question D:
subplot(3,1,1)
plot(posZ, angle(mFinalVect(:,1)),'LineWidth',2)
title('Slice Profile')
xlabel('Position')
ylabel('Phase')

subplot(3,1,2)
plot(posZ, abs(mFinalVect(:,1)),'LineWidth',2)
title('Slice Profile')
xlabel('Position')
ylabel('Amplitude')

subplot(3,1,3)
plot(posZ, abs(mFinalVect(:,2)),'LineWidth',2)
title('z-magnetization')
xlabel('Position')
ylabel('Amplitude')

print('Lab2p2_QuestionD','-dpng');

% Compare with reference
load('Lab2p1_ref.mat');

subplot(2,1,1);
plot(posZ, abs((mFinalVect(:,1))),'r--*',posZ,abs(Lab2p1_ref),'b');
title('comparision of magnitude between calculation and reference');
xlabel('Position');
ylabel('Magnitude');

subplot(2,1,2);
plot(posZ,angle((mFinalVect(:,1))),'r--o',posZ,angle(Lab2p1_ref));
title('comparison of phase between calculation and reference');
xlabel('Position');
ylabel('Phase');

print('Lab2p2_QuestionD_compare_ref','-dpng');
%Question E:

% Elapsed time is 3.790712 seconds, which is faster than the previous one.
