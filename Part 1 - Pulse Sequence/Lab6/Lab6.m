%% Lab 6

%Exercise 6.1 

% 6.1A: Describe the changes in the image contrast and explain 
%what may have caused them to be different in gray and white matter. 
%Why does the contrast change depending on the number of slices in the scan?

%Answer: As I run the code in test 4, we notice that, with the increase of 
%slice number, the contrast is attenuated. The signal from white mater 
%and gray matter is attenuated while the signal from the cerebrospinal 
%fluid is barely changed. The main reason for this is that compared to the 
%single slice excitation, multi-slice excitation creates off-resonant 
%saturation of bound pool magnetization in other slice, which will lead 
%to the signal reduction.

% 6.1B: where motion often leads to non-diagnostic image quality. In addition, 
% describe one situation where motion effects are exploited to help characterize the tissue.

%Answer: 

% 1)In most clinical scanning, patients should keep still during the scanning, 
% while sometimes the patients may involuntarily move the body or the head 
% which are being scanned. This will lead to motion ghost artefact that blurred the image. 

% 2) Cardiac MRI

% C: For each of the scanner imperfections listed above, describe one 
% possible artefact/problem that influence your study?

%Answer: My related research is diffusion MRI to study tissue microstructures.
% Recently, there is a technique called oscillating diffusion gradient with 
% varying diffusion time will give out detailed microstructure which is 
% restricted by water molecule diffusion. However, we know that rapid 
% switching of gradient will release enormous energy into the system, and 
% generated eddy current will spoil the perfect gradient, especially when 
% the diffusion time is short, in which case the two-diffusion gradient is 
% switch-on/ switch on in a very short period. This technique requires rapid 
% changing gradient that is subject to artefacts induced by the eddy current. 



