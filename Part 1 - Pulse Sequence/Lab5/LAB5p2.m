%% Exercise 5.2
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same

dt    = 10^-5; 
gamma = 42.577*10^6;

%load the voxel model
load('PD.mat');
load('T1.mat');
load('T2.mat');


%Allocate the memory needed
nTimeSteps  = 700;
rfPulse     = zeros(1,nTimeSteps); %variable to hold a RF waveform
rfPulseE    = zeros(1,nTimeSteps);
rfPulseR    = zeros(1,nTimeSteps);
gradAmp     = zeros(3,nTimeSteps); %variable to hold a gradient waveform
adc         = zeros(1,nTimeSteps); %variable to hold a gradient waveform
time        = zeros(1,nTimeSteps); %variable to hold the time points


xSteps  = size(T1,1);   %Number of simulated "spins" in the x directions 
ySteps  = size(T1,2);   %Number of simulated "spins" in the y directions 
zSteps  = 1;            %Number of simulated "spins" in the z directions 

dX = 4.0e-3;            %Distance between simulated "spins" in the x directions  [meter]
dY = 4.0e-3;            %Distance between simulated "spins" in the y directions  [meter]
dZ = 1.0e-4;            %Distance between simulated "spins" in the z directions  [meter]


% 3D positions in space
pos = zeros(3,xSteps,ySteps,zSteps);
for k=1:xSteps
    for j=1:ySteps
        for i=1:zSteps
            pos(1,k,j,i) = (k-xSteps/2)*dX;
            pos(2,k,j,i) = (j-ySteps/2)*dY;
            pos(3,k,j,i) = (i-zSteps/2)*dZ;
        end
    end
end

%Generates the time line for sequence plotting
for i=1:nTimeSteps 
    time(i)    = i*dt;                       %Time in seconds
end

%% Question A

TE = 6*1.0e-3; %6ms

%Generate the excitation pulse (90-degree):
area = 0;
FA = pi/2;
for i=1:100
    rfPulseE(i) = sinc(3000*(i-50)*dt);
    area = area + rfPulseE(i)*dt;
end

sinc_90_amp = FA/(2*pi*gamma*area);
rfPulseE = rfPulseE * sinc_90_amp;
%Generate the refocusing pulse (180-degree):
area2 = 0;
FA2 = pi;

for i = 300:400  % No actual time meaning, just use it to calculate altitude
rfPulseR(i) = sinc(3000*(i-350)*dt);
    area2 = area2 + rfPulseR(i)*dt;
end

sinc_180_amp = FA2/(2*pi*gamma*area2);
rfPulseR = rfPulseR * sinc_180_amp;

rfPulse = rfPulseE + rfPulseR;

plot(time,rfPulse)
xlabel('time/s');
ylabel('B1/T');
title('Spin Echo TE = 6ms');
print('SEsequence_Lab5p2','-dpng');

%Generate the readout + ADC:

Lx = 0.192;                            % Lx is FOV in x direction;
nx = 48;                               % nx is number of data point
Tacq = nx*dt;                          % Tacq is the acquisition time
BW_receiver = nx/(2*Tacq);            % Receiver Bandwidth
Gx = 4*pi*BW_receiver/(2*pi*gamma*Lx); % Gx = 0.0122 T/m

% Slice selection gradient
dz = 0.005;
df = 3000;
Gz = df/(gamma*dz); % Calculating Gz 

for i = 1:100
    gradAmp(3,i) = Gz;
end

for i = 300:400
    gradAmp(3,i) = Gz;
end

% Slice refocusing gradient
for i = 101:150
    gradAmp(3,i) = -Gz;
end

for i = 250:299
    gradAmp(3,i) = -Gz;
end

for i = 401:450
    gradAmp(3,i) = -Gz;
end

% ADC

adc = zeros(1,length(rfPulse)); % variable to hold an ADC 

for i = 1:nTimeSteps
    if i >= 626 && i < (626+size(T1,1)) %%%
        adc(i) = i-625;
    end
end

% phase encoding gradient calculation:
Ly = 0.192;                            % Ly is FOV in y direction
N = 48;                                % N is number of phase encoding step
Area_pe_max = pi*47/(2*pi*gamma*Ly);   % Area under the largest phase encoding
Gymax = Area_pe_max/(48*dt);       % gradAmp(2,i) = 0.0057 T/m (largest gradient)

% read out gradient and prephase gradient:

for i = 575:625
    gradAmp(1,i) = -0.5*Gx;
    gradAmp(2,i) = Gymax;
end

for i = 626:(626+size(T1,1))
    gradAmp(1,i) = Gx;
end

%Plot the compleate sequence

displaysequencediagram(time,rfPulse,gradAmp(1,:),gradAmp(2,:),gradAmp(3,:),adc);

%% Question B:

kSpace  = zeros(size(T1,1),size(T1,2));

tic
for trIndex=1:size(T1,2)

disp(trIndex);    

%Update phase encoding gradients

for t=101:149
      %gradAmp(2,t) =  pi*(2*(trIndex-24)-1)/(2*pi*gamma*Ly*50*dt); %Y gradients in Tesla per meter
      % gradAmp(2,t) = -Gymax + (trIndex-1)*2*Gymax/47;
      gradAmp(2,t) = Gymax - (trIndex-1)*Gymax/24;
end

for k=1:xSteps
    
    for j=1:ySteps
        for i=1:zSteps

            dB0 = pos(:, k, j, i)'*gradAmp(:,1);  % dot product between G(t).r
            [mT,mZ] =  bloch(dt, dB0,rfPulse(1), T1(k,j),T2(k,j), 0, 1);
    
            for t=2:nTimeSteps %i starts at 2

                dB0 = pos(:, k, j, i)'*gradAmp(:,t); 
                [mT,mZ] =  bloch(dt, dB0,rfPulse(t), T1(k,j),T2(k,j), mT, mZ);
                
                if(adc(t)>0)
                    kSpace(round(adc(t)),trIndex) = kSpace(round(adc(t)),trIndex) + mT * PD(k,j);
                end
                
            end            
            
            
        end
    end
end

end
toc

%% Reconstruct the image
clf;

imshow(abs(log(kSpace)),[]); % Show logarithmic scale of k-space data
print('Lab5p2_QuestionC_kspace_log','-dpng');
title('Spin Echo Lab5p2 logarithmic k-space');

clf;
imshow(abs(kSpace),[]);
print('Lab5p2_QuestionC_kspace','-dpng');
title('Spin Echo Lab5p2 raw k-space data');

clf;
i = ifftshift(ifft2(fftshift(kSpace)));
imshow(abs(i));
title('Spin Echo reconstructed image Lab5p2');
print('Lab5p2_QuestionC_FTimage','-dpng');

% Answer: This is a T2-weighted Image
