function  displaysequencediagram(time,rfPulse,Gx,Gy,Gz,adc)

subplot(5,1,1)
plot(time, rfPulse)
title('RF waveform')
xlabel('time')
ylabel('B1/T')

subplot(5,1,2)
plot(time, Gx)
title('X Gradient')
xlabel('time')
ylabel('T/m')

subplot(5,1,3)
plot(time, Gy)
title('Y Gradient')
xlabel('time')
ylabel('T/m')

subplot(5,1,4)
plot(time, Gz)
title('Z Gradient')
xlabel('time')
ylabel('T/m')

subplot(5,1,5)
plot(time, adc)
title('ADC')
xlabel('time')

end

