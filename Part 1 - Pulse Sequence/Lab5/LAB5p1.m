%% Exercise 5.1
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same


dt    = 10^-5; 
gamma = 42.577*10^6;

te = [6 8 12 16 24 32 48 64 96 128 192 256]*10^-3; % te in ms

%Question A

%Allocate the memory needed
nTimeSteps  = 70000;
rfPulseE    = zeros(1,200); %exciation
rfPulseR    = zeros(1,200); %refcosuing
rfPulse     = zeros(1,nTimeSteps); %variable to hold a RF waveform with 10000 samples
gradAmp     = zeros(3,nTimeSteps); %variable to hold a gradient waveform with 10000 samples
time        = zeros(1,nTimeSteps); %variable to hold 10000 time points

tiSteps    = size(te,2);
nSpins     = 1000;
spins      = zeros(1,nSpins); %variable to hold nPosSteps positions allong the z direction
mFinalVect = zeros(tiSteps,2); %variable to hold the final magnetization calculated for each position
tmp        = zeros(nSpins,2);

%Generates the time line for plotting
for i=1:nTimeSteps %i starts at 1 go's to 15000
    time(i)    = i*dt;                       %Time in seconds
end

%Generate off-resonances 
for i=1:nSpins
    spins(i) = 2.5*(rand-0.5)*10^-7;     
end

%Question A:

%Generate the RF excitation waveform (90 degree RF pulse):
% Known parameter for calculating: T = 0.001s; TBWP = 3, 1/t0 = 3000

area = 0;
FA = pi/2;
for i=1:100
    rfPulseE(i) = sinc(3000*(i-50)*dt);
    area = area + rfPulseE(i)*dt;
end

sinc_90_amp = FA/(2*pi*gamma*area);
rfPulseE = rfPulseE * sinc_90_amp;

%Generate the refocusing waveform (180 degree RF pulse):
area2 = 0;
FA2 = pi;
for i = 1:100  % No actual time meaning, just use it to calculate altitude
rfPulseR(i) = sinc(3000*(i-50)*dt);
    area2 = area2 + rfPulseR(i)*dt;
end

sinc_180_amp = FA2/(2*pi*gamma*area2);
rfPulseR = rfPulseR * sinc_180_amp;

%% Question B:

for measIndex = 1:size(te,2)
    for i = 1:nTimeSteps
        rfPulse(i) = 0;
    end
%Generate the sequence
    startE = 1;
    for i = 1:100
        rfPulse(i) = sinc_90_amp*sinc(3000*(i-50)*dt);
    end
    
    for i = (10^5*te(measIndex)/2):(100+10^5*te(measIndex)/2)
        rfPulse(i) = sinc_180_amp*sinc(3000*(i-(50+10^5*te(measIndex)/2))*dt);
    end
    %simulate the sequence
    for spinIndex=1:nSpins
        gradAtPosJ = spins(spinIndex);
        [mT,mZ] =  bloch(dt, gradAtPosJ,rfPulse(1), 0.85, 0.06, 0, 1);
        for i=2:(50 + 10^5*te(measIndex)) %i starts at 2
            [mT,mZ] =  bloch(dt, gradAtPosJ,rfPulse(i), 0.85, 0.06, mT, mZ);
        end
        tmp(spinIndex,:) = [mT,mZ];
    end
    
    mFinalVect(measIndex,1) = sum(tmp(:,1));
    mFinalVect(measIndex,2) = sum(tmp(:,2));

end
%% Plot the measured signal and phase as function of TE.
clf;

subplot(2,1,1);
plot(te,abs(mFinalVect(:,1)),'*');
xlabel('Echo Time/s');
ylabel('Amplitude of MR signal (1000 spins)');
title('Echo Time - Mxy Plot with 1000 spins');

subplot(2,1,2);
plot(te,angle(mFinalVect(:,1)),'+');
xlabel('Echo Time/s');
ylabel('Phase of MR signal (1000 spins)');
title('Echo Time - Mxy phase with 1000 spins')
ylim([-pi,pi]);

print('Lab5p1_QuetionB','-dpng');

tmp2 = abs(mFinalVect(:,1));

%% Question C: fitting the data to estimate T2

[xData, yData] = prepareCurveData( te, tmp2 );

% Set up fittype and options.
ft = fittype( '1000*exp(-b*x)', 'independent', 'x', 'dependent', 'y' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = 0.0776135886228111;

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );

% Plot fit with data.
figure( 'Name', 'untitled fit 1' );
h = plot( fitresult, xData, yData );
legend( h, 'tmp vs. te', 'exp(-t/T2)', 'Location', 'NorthEast' );
% Label axes
xlabel te
ylabel tmp
grid on

print('Lab5p1_QuestionC','-dpng');
% As the cftool indicates, the T2 (in fitting the parameter is b)is 1/16.62 = 0.06s, very well match the
% ground truth.