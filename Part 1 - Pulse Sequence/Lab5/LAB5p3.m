%% Exercise 5.3: Turbon Spin Echo (TSE)
clear all; clc; close all; % clean up

tmp = matlab.desktop.editor.getActive;  % get location of this script
cd(fileparts(tmp.Filename));            % set working directory to same

dt    = 10^-5; 
gamma = 42.577*10^6;

%load the voxel model
load('PD.mat');
load('T1.mat');
load('T2.mat');

%Allocate the memory needed
nTimeSteps  = 14400;
rfPulse     = zeros(1,nTimeSteps); %variable to hold a RF waveform
rfPulseE    = zeros(1,nTimeSteps);
rfPulseR    = zeros(1,nTimeSteps);
gradAmp     = zeros(3,nTimeSteps); %variable to hold a gradient waveform
adc         = zeros(2,nTimeSteps); %variable to hold a gradient waveform
time        = zeros(1,nTimeSteps); %variable to hold the time points


xSteps  = size(T1,1);   %Number of simulated "spins" in the x directions 
ySteps  = size(T1,2);   %Number of simulated "spins" in the y directions 
zSteps  = 1;            %Number of simulated "spins" in the z directions 

dX = 4.0e-3;            %Distance between simulated "spins" in the x directions  [meter]
dY = 4.0e-3;            %Distance between simulated "spins" in the y directions  [meter]
dZ = 1.0e-4;            %Distance between simulated "spins" in the z directions  [meter]


% 3D positions in space
pos = zeros(3,xSteps,ySteps,zSteps);
for k=1:xSteps
    for j=1:ySteps
        for i=1:zSteps
            pos(1,k,j,i) = (k-xSteps/2)*dX;
            pos(2,k,j,i) = (j-ySteps/2)*dY;
            pos(3,k,j,i) = (i-zSteps/2)*dZ;
        end
    end
end

%Generates the time line for sequence plotting
for i=1:nTimeSteps 
    time(i)    = i*dt;                       %Time in seconds
end

%% Question A

dt1 = 0.006;
tp1 = dt1*10^5;  % tp1 = 600;
dt2 = (24+50+50)*10^-5;
% In my guess, the most appropriate case for allocating shortest dt2 is
% leave prephasor same time point as the first prephasing, which is means
% 50 time points, 24 is half the readout, 50 is half the 180 degree pulse.
tp2 = dt2*10^5;  %tp2 = 124;

%Generate the excitation(s) 
area = 0;
FA = pi/2;
hanning_window = hann(100)';

for i=1:100
    rfPulseE(i) = hanning_window(i)*sinc(3000*(i-50)*dt);
    area = area + rfPulseE(i)*dt;
end

sinc_90_amp = FA/(2*pi*gamma*area);
rfPulseE = rfPulseE * sinc_90_amp;

%Generate the refocusing waveform (180 degree RF pulse):
area2 = 0;
FA2 = pi;

for i = (50+tp1-50+1):(tp1+100)    % i = 601:700
    rfPulseR(i) = hanning_window(i-tp1)*sinc(3000*(i-(tp1+50))*dt);
    area2 = area2 + rfPulseR(i)*dt;
end

sinc_180_amp = FA2/(2*pi*gamma*area2);
rfPulseR = rfPulseR * sinc_180_amp;

% Generate the second 180 pulse with dt2:
for i = (50+2*tp1+tp2-49):(50+2*tp1+tp2+50)  % i = 1325?1424
    rfPulseR(i) = hanning_window(i-(50+2*tp1+tp2-50))*sinc_180_amp*sinc(3000*(i-(50+2*tp1+tp2))*dt);
end

% This section is to generate the first FSE sequence that can be used to
% iterate the rf Pulse later.

%% Slice selection gradient
dz = 0.005;
df = 3000;
Gz = df/(gamma*dz); % Calculating Gz = 0.0141 T/m 

% Readout(frequency-encoding gradient)

Lx = 0.192;                            % Lx is FOV in x direction;
nx = 48;                               % nx is number of data point
Tacq = nx*dt;                          % Tacq is the acquisition time
BW_receiver = nx/(2*Tacq);             % Receiver Bandwidth
Gx = 4*pi*BW_receiver/(2*pi*gamma*Lx); % Gx = 0.0122 T/m

% phase encoding gradient calculation:
Ly = 0.192;                            % Ly is FOV in y direction
N = 48;                                % N is number of phase encoding step
Area_pe_max = pi*48/(2*pi*gamma*Ly);   % Area under the largest phase encoding
Gymax = Area_pe_max/(50*dt);       % gradAmp(2,i) = 0.0057 T/m (largest gradient)

% Generate the slice selective gradient:
for i = 1:100
    gradAmp(3,i) = Gz;
end

for i = (tp1+1):(tp1+100)   % i = 601:700
    gradAmp(3,i) = Gz;
end

for i = (50+2*tp1+tp2-49):(50+2*tp1+tp2+50) % i =1325:1424
    gradAmp(3,i) = Gz;
end

% Generate the slice refocusing gradient:
% 90 degree pulse slice refocusing:
for i = 101:150
    gradAmp(3,i) = -Gz;
end

% 180 degree pulse slice refocusing
for i = (tp1+50-50-50+1):(tp1+50-50)     % i = 551:600   
    gradAmp(3,i) = -Gz;
end
    
for i = (tp1+101):(tp1+150)           % i = 701:750
    gradAmp(3,i) = -Gz;
end

% Generate the readout + ADC
for i = (50+2*tp1-24+1):(50+2*tp1-24+size(T1,1))    % i =1227:1274
    gradAmp(1,i) = Gx;
    adc(1,i) = i-(50+2*tp1-24);
    adc(2,i) = 1;
end

for i = (50+2*tp1+2*tp2-24+1):(50+2*tp1+2*tp2+24) % i = 1475:1522
    gradAmp(1,i) = Gx;
    adc(1,i) = i-(50+2*tp1+2*tp2-24);
    adc(2,i) = 2;
end

% Generate Phase encoding Gradient/prephasor gradient:
for i = (50+2*tp1-24-50+1):(50+2*tp1-24-50+50)   % i = 1177?1226
    gradAmp(2,i) = Gymax;
    gradAmp(1,i) = -24*Gx/50;
end

for i = (50+2*tp1+24+1):(50+2*tp1+24+50)  % i = 1275:1324
    gradAmp(1,i) = -24*Gx/50;
    gradAmp(2,i) = -Gymax;
    gradAmp(3,i) = -Gz;
end

for i = (50+2*tp1+tp2+50+1):(50+2*tp1+tp2+50+50) % i = 1425:1474
    gradAmp(1,i) = -24*Gx/50;
    gradAmp(2,i) = Gymax - 2*Gymax/47;
    gradAmp(3,i) = -Gz;
end

rfPulse = rfPulseE + rfPulseR;
%% Turbo Spin Factor Loop (remaining 47 180 degree pulse and gradient)
    
for i=1:46                          

    nStart = (50+2*tp1+24+1) + (i)*(48+50+100+50);  % 1st start: 1275
    % interval of each TR is 248 
    nEnd = (50+2*tp1+2*tp2+24) + (i)*(48+50+100+50);% 1st end: 1522
    
    rfPulse(:,nStart:nEnd) = rfPulse(:,(50+2*tp1+24+1):(50+2*tp1+2*tp2+24));
 
    gradAmp(1,nStart:nEnd) = gradAmp(1,(50+2*tp1+24+1):(50+2*tp1+2*tp2+24)); 
    
    % To apply phase encoding gradient before every Readout:
    gradAmp(2,nStart+50+100:nStart+50+149) = gradAmp(2,(50+2*tp1+tp2+50+1):(50+2*tp1+tp2+50+50)) - 2*i*Gymax/47;
    
    % To balance all previous phase encoding gradient:
    gradAmp(2,nStart:nStart+49) = gradAmp(2,(50+2*tp1+24+1):(50+2*tp1+24+50)) + 2*i*Gymax/47;
    
    gradAmp(3,nStart:nEnd) = gradAmp(3,(50+2*tp1+24+1):(50+2*tp1+2*tp2+24));
   
    adc(1,nStart:nEnd)     = adc(1,(50+2*tp1+24+1):(50+2*tp1+2*tp2+24));
    adc(2,nStart:nEnd)     = adc(2,(50+2*tp1+24+1):(50+2*tp1+2*tp2+24))*(i+2)/2;
end

% Plot the compleate sequence

displaysequencediagram_2adc(time,rfPulse,gradAmp(1,:),gradAmp(2,:),gradAmp(3,:),adc(1,:),adc(2,:));

%% Question B:


kSpace  = zeros(size(T1,1),size(T1,2));

tic
    
for k=1:xSteps
    disp(k);
    for j=1:ySteps
        for i=1:zSteps
            
            dB0 = pos(:, k, j, i)'*gradAmp(:,1);
            [mT,mZ] =  bloch(dt, dB0,rfPulse(1), T1(k,j),T2(k,j), 0, 1);
    
            for t=2:nTimeSteps %i starts at 2

               dB0 = pos(:, k, j, i)'*gradAmp(:,t);
               [mT,mZ] = bloch(dt, dB0,rfPulse(t), T1(k,j),T2(k,j), mT, mZ);
                
                if(adc(1,t)>0)
                    if mod(adc(2,t),2)>0
                    kSpace(round(adc(1,t)),round(adc(2,t))) = kSpace(round(adc(1,t)),round(adc(2,t))) + mT * PD(k,j);
                    else
                    kSpace(round(adc(1,t)),round(adc(2,t))) = kSpace(round(adc(1,t)),round(adc(2,t))) - mT * PD(k,j);
                    end
                end
                
            end            
            
            
        end
    end
end

toc

%% Question C: 
clf;

imshow(abs(log(kSpace)),[]); % Show logarithmic scale of k-space data

print('Lab5p3_QuestionC_kspace_log_TSE','-dpng');
clf;

imshow(abs(kSpace),[]);

print('Lab5p3_QuestionC_kspace_TSE','-dpng');

clf;

im = ifftshift(ifft2(fftshift(kSpace)));

imshow(abs(im),[]);

print('Lab5p3_QuestionC_FTimage_TSE','-dpng');

% Answer: This is a T2-weighted image.
%% Question D:

% Plot both image reconstructed in Lab5.2 and Lab5.3:
load('lab5_2kspace.mat');
clf;

subplot(1,2,1);
ima = ifftshift(ifft2(fftshift(lab5_2kspace)));
title('Reconstructed Image form 5.2');

subplot(1,2,2);
im = ifftshift(ifft2(fftshift(kSpace)));
% iml = im(:,1:24);
% imr = im(:,25:48);
% imre = cat(2,imr,iml);
imshow(abs(im),[]);
title('Reconstructed Image from 5.3');

print('Lab5p3_QuestionC_FTimage_compare5.2&5.3','-dpng');

% Answer: The image constructed by Spin Echo has higher signal intensity, because
% the turbo spin echo has utilize the magnetization with maximum
% efficiency, therefore the transverse magnetization is not uniform (signal 
% intensity decreased gradually), and the amplitude is decreasing gradually
% with each TR loop. Therefore, the picture is not as good as the 5.2 but it is still acceptable.
%% Question E:
% in order to obtain a center-out ordering, we need to change the phase
% encoding gradient, in the previous section, we sampled the k-space by
% following the center-in ordering, therefore the image appear clearer at
% the peripheral of the image.
